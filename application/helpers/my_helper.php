<?php

function ddq($var1 = "here")
{
    echo "<pre>" . ($var1);

    exit;
}

function dd($var1 = "here", $line = null, $file = null)
{
    if (is_null($line) and is_null($file)) {
        var_dump($var1);

    } else {
        var_dump($var1);

        echo "at line : $line in file $file";
    }
    exit;
}

/*function Redirect($uri, $httpResponseCode = 302)
{
    if (!preg_match('#^https?://#i', $uri)) {
        $uri = base_url() . $uri;
        header("Location: " . $uri, TRUE, $httpResponseCode);
        die();
    }
}*/

function generateRandomAlphaNumericString($length = 7)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomString($length = 12)
{
    $characters = '123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function translate($key) {
    $ci = & get_instance();
    echo $ci->lang->line($key);
}

function get_page($key, $value) {
    $ci = & get_instance();
    $page = $ci->common_model->get_where('tbl_pages', array($key => $value));
    if ($page) {
        return $page[0];
    } else {
        return false;
    }
}

function successflash() {
    $ci = & get_instance();
    if ($ci->session->flashdata('msg')) {
        echo
        '<div class="alert alert-success alert-dismissible fade in" style="position: absolute;z-index: 900;bottom: 60px;right: 10px;background: rgba(136,185,60,0.8) !important;" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
        . $ci->session->flashdata('msg') .
        '</div>';
    }
}

function failedflash() {
    $ci = & get_instance();
    if ($ci->session->flashdata('msg')) {
        echo
            '<div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
            . $ci->session->flashdata('msg') .
            '</div>';
    }
}

function check_user($check_index = 'current_user') {
    $ci = & get_instance();
    return ($ci->session->userdata($check_index)) ? $ci->session->userdata($check_index) : false;
}

function check_front_user() {
    $ci = & get_instance();
    return ($ci->session->userdata('front_user')) ? $ci->session->userdata('front_user') : false;
}

function get_userdata($index) {
    $ci = & get_instance();
    return $ci->session->userdata($index) ? $ci->session->userdata($index) : false;
}

function php_thumb_image($url, $file, $width, $height, $crop = false, $fall_back_image) {
    $filename = $file != '' && file_exists($url . $file) ? $file : $fall_back_image;
    $path = base_url(substr($url, 2) . $filename);
    echo
    '
        <img src="' . base_url() . 'assets/phpthumb/phpThumb.php?src=' . urlencode($path) . '&amp;w=' . $width . '&amp;h=' . $height . '&amp;zc=' . $crop . '">
    ';
}

function debug($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    die;
}

function sendemail($params) {
    //Filtering @params
    $params['from_name'] = (!isset($params['from_name'])) ? '' : $params['from_name'];
    $params['replytoname'] = (!isset($params['replytoname'])) ? '' : $params['replytoname'];

    $ci = & get_instance();

    //Loading email library
    $ci->load->library('email');
    $mail = $ci->email;
    $mail->initialize(array('mailtype' => 'html'));

    //Set who the message is to be sent from
    $mail->from($params['from'], $params['from_name']);

    //Set an alternative reply-to address
    $mail->reply_to($params['replyto'], $params['replytoname']);

    //Set who the message is to be sent to
    $mail->to($params['sendto']);

    if (isset($params['bcc'])) {
        //Set the BCC address
        $mail->bcc($params['bcc']);
    }

    if (isset($params['cc'])) {
        //Set the CC address
        $mail->bcc($params['cc']);
    }

    //Set the subject line
    $mail->subject($params['subject']);

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->message($params['message']);

    //send the message, check for errors
    if (!$mail->send()) {
        echo $mail->print_debugger();
        die;
    } else {
        return true;
    }
}

function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}

function get_user($id) {
    $ci = & get_instance();
    $q = $ci->db->get_where('tbl_user', array('id' => $id))->result_array();
    if ($q)
        return $q[0];
    else
        return false;
}

function segment($seg) {
    $ci = & get_instance();
    return $ci->uri->segment($seg);
}

function get_care_detail($care_type) {
    $ci = & get_instance();
    $q = $ci->db->get_where('tbl_care', array('id' => $care_type))->result_array();
    return $q[0];
}

function get_care() {
    $ci = & get_instance();
    $ci->db->order_by('service_by ASC');
    $q = $ci->db->get_where('tbl_care')->result_array();
    return $q;
}

function get_unread_ticket_count() {
    $ci = & get_instance();
    $ci->db->like('view', '0');
    $ci->db->from('tbl_tickets_history');
    return $ci->db->count_all_results();
}

function encrypt_decrypt($action, $string) {
    $output = false;

    $key = '!@#$%^&*';

    // initialization vector
    $iv = md5(md5($key));

    if ($action == 'encrypt') {
        $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, $iv);
        $output = rtrim($output, "");
    }
    return $output;
}

function home_flash() {
    $ci = & get_instance();
    if ($ci->session->flashdata('msg')) {
        echo '<script>$.jGrowl("' . $ci->session->flashdata('msg') . '")</script>';
    }
}

function set_flash($index, $msg) {
    $ci = & get_instance();
    $ci->session->set_flashdata($index, $msg);
}

function generateString() {
    $number = base64_encode(random_string('alnum', 20));
    $newstr = preg_replace('/[^a-zA-Z0-9\']/', '', $number);
    return substr($newstr, 0, 10); // return string of 10 random character
}

function swiftsend($param) {
    include_once APPPATH . 'third_party/swiftmailer/swift_required.php';

    $numSent = 0; // Var to store the number of successful email recipients

    if ($_SERVER['HTTP_HOST'] == 'localhost') {
        ini_set("SMTP", "ssl://smtp.gmail.com");
        ini_set("smtp_port", "465");
        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', '465', 'ssl')
                ->setUsername(SWIFT_EMAIL)
                ->setPassword(SWIFT_PASS);
    } else {
        $transport = Swift_SmtpTransport::newInstance();
    }

    $mailer = Swift_Mailer::newInstance($transport);

    $message = Swift_Message::newInstance($param['subject'])
            ->setFrom($param['from'])
            ->setReplyTo($param['reply'])
            ->setBody($param['msg'], 'text/html');

    // Sending bulk email such that the people in the list will not be aware of eachother
    foreach ($param['recipients'] as $address => $name) {
        if (is_int($address)) {
            $message->setTo($name);
        } else {
            $message->setTo(array($address => $name));
        }

        $numSent += $mailer->send($message, $failedRecipients);
    }
    $return = array('sent' => $numSent, 'failed' => $failedRecipients);

    return (isset($param['return']) && $param['return']) ? $return : true;
}

function uniqueid($prefix, $length = 4, $search_table, $search_column) {
    $ci = &get_instance();
    $id = $prefix . substr(rand(1111111111, 9999999999), 0, $length);
    $checks = $ci->db->get_where($search_table, array($search_column => $id));
    if ($checks->num_rows() > 0)
        uniqueid($prefix, $length, $search_table, $search_column);
    else
        return $id;
}


function getCurrentDateTime(){
    $t= time();
    return (date("Y-m-d",$t));
}



