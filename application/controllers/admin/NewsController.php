<?php
use Ministry\Model\AjaxGrid;
use Ministry\Model\News;
use Ministry\Repositories\NewsRepository;


class NewsController extends CI_Controller
{

    private $newsRepository;

    public function __construct()
    {
        parent::__construct();
        if (!check_user('current_user')) {
            redirect('admin');
        }

        $this->newsRepository = new NewsRepository();

    }

    public function IndexAction()
    {
        $this->load->view('admin/news/index');
    }

    public function ListAction()
    {
        $ajaxGrid = new AjaxGrid();

        $ajaxGrid->MapParameters($_GET);

        echo json_encode($this->newsRepository->FindAll($ajaxGrid));
    }

    function UpdateImage()
    {
        if (isset($_FILES["upload_image"]) && !empty($_FILES["upload_image"]["name"]) && $_FILES["upload_image"]["size"] > 0) {

            $parts = explode(".", $_FILES["upload_image"]["name"]);

            $filename = $parts[0] . generateRandomAlphaNumericString() . "." . end($parts);

            $destination = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "image_link" . DIRECTORY_SEPARATOR . $filename;

            $uploaded = move_uploaded_file($_FILES["upload_image"]["tmp_name"], $destination);

            if ($uploaded) return $filename;
        }
    }

    public function SaveUpdateAction()
    {

        $news = new News();

        $news->MapParameters($_POST);

        $news->date = substr($news->date, 0, 9);
        if (strlen($news->date) > 0)
            $news->date = date("Y-m-d", strtotime($news->date));

        if (isset($news->ID) && strlen($news->ID) > 0) {

            if (isset($_FILES["upload_image"]) && !empty($_FILES["upload_image"]["name"]) && $_FILES["upload_image"]["size"] > 0) {
                $oldNews = $this->newsRepository->GetNewsById($news->ID);

                $fullFilePath = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "image_link" . DIRECTORY_SEPARATOR . $oldNews->image_link;

                if (file_exists($fullFilePath)) {
                    unlink($fullFilePath);
                }

                $news->image_link = $this->UpdateImage();

                $this->newsRepository->UpdateNews($news);
            } else {
                $this->newsRepository->UpdateNews($news, true);
            }


        } else {



            $news->image_link = $this->UpdateImage();

            $this->newsRepository->SaveNews($news);
        }

        redirect("admin/news");
    }

    public function FormAction()
    {
        $id = isset($_POST['ID']) ? $_POST['ID'] : '';

        $params = array();

        $params['News'] = new News();

        $params['SaveUpdate'] = "Save";

        if (isset($id) && strlen($id) > 0) {

            $params['News'] = $this->newsRepository->GetNewsById($id);

            $params['SaveUpdate'] = "Update";
        }

        $this->load->view('admin/news/news_edit', $params);
    }

    public function DeleteAction()
    {
        $data = $this->input->post();

        $this->newsRepository->DeleteNews($data['ID']);

        redirect("admin/news");

    }

} 