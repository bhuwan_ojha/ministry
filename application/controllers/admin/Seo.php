<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if(!check_user('current_user')) {
            redirect('admin');
        }
        $this->CommonModel = new Common_Model();
    }

    function index()
    {
        $seo = $this->CommonModel->get_all('ministry_seo_table', '', 'id DESC');
        $this->load->view('admin/seo-manager', array('seo'=>$seo));
    }

    function form(){
        $this->load->view('admin/seo-form');
    }

    function add_update()
    {
        $seo_id = segment(3);
        if($this->input->post()) {
            $post = array(
                'name'=> $this->input->post('name'),
                'link'=> $this->input->post('link'),
                'title'=> $this->input->post('title'),
                'description'=> $this->input->post('description'),
                'keywords'=> $this->input->post('keywords')
            );
            if(($this->input->post('id'))=='') {
                $this->CommonModel->insert('ministry_seo_table', $post);
                set_flash('msg', 'SEO Details Saved');
            } else {
                $this->CommonModel->update('ministry_seo_table', $post, array('id' => $this->input->post('id')));
            }
            set_flash('msg', 'SEO details Updated');
            redirect('admin/seo');
        } else{
                $data= $this->CommonModel->get_where('ministry_seo_table', array('id' => $seo_id));
                $this->load->view('admin/seo-form',array('seo'=>$data));
            }


    }

    function delete()
    {

        $this->CommonModel->delete_data('ministry_seo_table', array('id' => $this->input->post('id')));
        set_flash('msg', 'SEO Details Deleted');
        redirect('admin/seo');
    }

    function common(){

        if($this->input->post()) {
        $post = $this->input->post();
            $this->CommonModel->update('ministry_seo_common_table', $post, array('id' => 1));
            set_flash('msg', 'Common SEO Details Updated');
        }

        $data = $this->CommonModel->get_where('ministry_seo_common_table', array('id' => 1));
        $this->load->view('admin/meta-tag', array('data'=> $data));

    }
}