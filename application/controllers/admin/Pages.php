<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/20/2016
 * Time: 11:04 AM
 */

class Pages extends CI_Controller {

    private $CommonModel;
    private $CommonLibrary;

    public function __construct(){
        parent::__construct();
        if(!check_user('current_user')) {
            redirect('admin');
        }
        $this->CommonModel = new Common_Model();
        $this->CommonLibrary = new Common_Library();
;
        if(!check_user('current_user')&& segment(2) !='logout'){

        }
    }

    public function index(){
        $data = $this->CommonModel->get_all('ministry_pages_table','');

        $this->load->view('admin/page-list',array('data'=> $data));
    }

    public function delete(){
        $pageData = $this->input->post();
        $data = $this->CommonModel->get_where('ministry_pages_table',array('id' => $pageData['id']));
        unlink("uploads/".$data[0]['page_image']);
        $result = $this->CommonModel->delete_data('ministry_pages_table',array('id'=> $pageData['id']));
        if($result){
            set_flash('msg','Successfully Deleted Page');
        }else{
            set_flash('msg','Failed Deleting Page');
        }
    }

    public function form(){
        $this->load->view('admin/form');
    }

    public function formEdit(){
        $id = segment(3);
        $data =   $this->CommonModel->get_where('ministry_pages_table',array('id' => $id));
        $this->load->view('admin/form',array('data'=> $data));


    }

    public function SaveUpdate(){

        $image = $_FILES['image'];

        if ($_FILES['image']['size'] !== 0 && $_FILES['image']['error'] == 0)
        {
            $pageImage = $this->CommonModel->get_where('ministry_pages_table', array('id' => $this->input->post('id')));
            if($pageImage) {
                $url = 'uploads/'.$pageImage[0]['page_image'];
                if(file_exists($url))
                    unlink($url);
            }
            $upload = $this->CommonLibrary->upload($image);
            $page['page_image'] = $upload;            }
            $page['page_title'] = $this->input->post('page_title');
            $page['page_title_ne'] = $this->input->post('page_title_ne');
            $page['page_description'] = $this->input->post('editor1');
            $page['page_description_ne'] = $this->input->post('editor1_ne');

            $page['created_date'] = getCurrentDateTime();
        if($this->input->post('id')) {
            $result = $this->CommonModel->update('ministry_pages_table', $page,array('id'=>$this->input->post('id')));

            if($result==TRUE){
                set_flash('msg','Successfully Updated Page');
            }else{
                set_flash('msg','Failed Updating Page');
            }
        }else{
            $result = $this->CommonModel->insert('ministry_pages_table', $page);
        }
            if($result==TRUE){
                set_flash('msg','Successfully Added Page');
            }else{
                set_flash('msg','Failed Adding Page');
            }



        redirect('admin/pages');

    }

} 