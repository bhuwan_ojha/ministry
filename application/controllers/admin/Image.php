<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/23/2016
 * Time: 9:56 AM
 */

class Image extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!check_user('current_user')) {
            redirect('admin');
        }

        $this->CommonModel = new Common_Model();
        $this->CommonLibrary = new Common_Library();
    }


    function index(){
        $images = $this->CommonModel->get_all('ministry_image_table');
        $this->load->view('admin/image-manager',array('images' => $images));
    }


    function addImage(){
       $image = $_FILES['file'];
        $upload = $this->CommonLibrary->upload($image);
        if($upload){
            $data = array(
              'image' => $upload,
                'created_date' => getCurrentDateTime(),
                'status' => 0
            );
            $this->CommonModel->insert('ministry_image_table',$data);
        }
        redirect('admin/image');
    }




    function delete(){
        $imageID = $this->input->post('id');
        $banner = $this->common_model->get_where('ministry_image_table', array('id' => $imageID));
        unlink("uploads/".$banner[0]['image']);

        $this->common_model->delete_data('ministry_image_table', array('id' => $imageID));
        return set_flash('msg', 'Successfully Deleted Image');

    }

    function update(){
        $data = array(
            'id'=> $this->input->post('id'),
            'status' => $this->input->post('status'),
        );

        $this->CommonModel->update('ministry_image_table',$data,array('id'=>$_POST['id']));

    }

} 