<?php
use Ministry\Model\AjaxGrid;
use Ministry\Model\Publication;
use Ministry\Repositories\PublicationRepository;


class PublicationController extends CI_Controller
{

    private $publicationRepository;

    public function __construct()
    {
        parent::__construct();
        if (!check_user('current_user')) {
            redirect('admin');
        }

        $this->publicationRepository = new PublicationRepository();

    }

    public function IndexAction()
    {
        $this->load->view('admin/publication/index');
    }

    public function ListAction()
    {
        $ajaxGrid = new AjaxGrid();

        $ajaxGrid->MapParameters($_GET);

        echo json_encode($this->publicationRepository->FindAll($ajaxGrid));
    }

    function generateRandomAlphaNumericString($length = 7)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function SaveUpdateAction()
    {
        $publication = new Publication();

        $publication->MapParameters($_POST);

        $publication->date = substr($publication->date, 0, 9);
        if (strlen($publication->date) > 0)
            $publication->date = date("Y-m-d", strtotime($publication->date));

        if (isset($publication->ID) && strlen($publication->ID) > 0) {

            $oldPublication = $this->publicationRepository->GetPublicationById($publication->ID);

            $media = false;
            $image = false;
            if (isset($_FILES["media_link"]) && !empty($_FILES["media_link"]["name"]) && $_FILES["media_link"]["size"] > 0) {

                $fullFilePath = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "publication" . DIRECTORY_SEPARATOR . $oldPublication->media_file_name;
                if (file_exists($fullFilePath)) {
                    unlink($fullFilePath);
                }

                $publication->media_file_name = $this->UpdateFile();
                $media = true;

            }

            if (isset($_FILES["image_link"]) && !empty($_FILES["image_link"]["name"]) && $_FILES["image_link"]["size"] > 0) {

                $fullFilePath = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "image_link" . DIRECTORY_SEPARATOR . $oldPublication->image_file_name;
                if (file_exists($fullFilePath)) {
                    unlink($fullFilePath);
                }

                $publication->image_file_name = $this->UpdateImage();
                $image = true;


            }

            $this->publicationRepository->UpdatePublication($publication, $media, $image);

        } else {
            //save part

            $publication->media_file_name = $this->UpdateFile();

            $publication->image_file_name = $this->UpdateImage();

            $this->publicationRepository->SavePublication($publication);
        }


        redirect("admin/publication");
    }

    function UpdateFile()
    {
        if (isset($_FILES["media_link"]) && !empty($_FILES["media_link"]["name"]) && $_FILES["media_link"]["size"] > 0) {

            $parts = explode(".", $_FILES["media_link"]["name"]);

            $filename = $parts[0] . generateRandomAlphaNumericString() . "." . end($parts);

            $destination = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "publication" . DIRECTORY_SEPARATOR . $filename;

            $uploaded = move_uploaded_file($_FILES["media_link"]["tmp_name"], $destination);

            if ($uploaded) return $filename;
        }
    }

    function UpdateImage()
    {
        if (isset($_FILES["image_link"]) && !empty($_FILES["image_link"]["name"]) && $_FILES["image_link"]["size"] > 0) {

            $parts = explode(".", $_FILES["image_link"]["name"]);

            $filename = $parts[0] . generateRandomAlphaNumericString() . "." . end($parts);

            $destination = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "image_link" . DIRECTORY_SEPARATOR . $filename;

            $uploaded = move_uploaded_file($_FILES["image_link"]["tmp_name"], $destination);

            if ($uploaded) return $filename;
        }
    }

    public function FormAction()
    {
        $id = isset($_GET['ID']) ? $_GET['ID'] : '';

        $params = array();

        $params['Publication'] = new Publication();

        $params['SaveUpdate'] = "Save";

        if (isset($id) && strlen($id) > 0) {

            $params['Publication'] = $this->publicationRepository->GetPublicationById($id);

            $params['SaveUpdate'] = "Update";
        }

        $this->load->view('admin/publication/form', $params);
    }

    public function DeleteAction()
    {
        $data = $this->input->post();

        $this->publicationRepository->DeletePublication($data['ID']);

        redirect("admin/publication");

    }

} 