<?php

use Ministry\Model\AjaxGrid;
use Ministry\Repositories\PublicationRepository;

class PublicationController extends CI_Controller
{

    private $publicationRepository;

    private $CommonModel;

    function __construct()
    {
        parent::__construct();

        $this->publicationRepository = new PublicationRepository();

        $this->CommonModel=new Common_Model();
    }

    public function IndexAction()
    {
        $params['TotalCount'] = $this->publicationRepository->RowCount();

        $this->load->view('publication', $params);
    }

    public function ListAction()
    {

        $page = isset($_GET['page']) ? $_GET['page'] : '1';
        $perPage = isset($_GET['perPage']) ? $_GET['perPage'] : '5';
        echo json_encode($this->publicationRepository->GetDataByPage($page,$perPage));
    }

    public function ShowDetailAction()
    {
        $id=$_POST['id'];

        $Publication = $this->publicationRepository->GetFulldata($id);

        $fullFilePath = BASEPATH . ".." . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "publication" . DIRECTORY_SEPARATOR . $Publication->media_file_name;

        $filename = $Publication->media_file_name;
        $filename = explode(".", $filename);
        $filename = substr($filename[0], 0, -7) . "." . end($filename);

        if(file_exists($fullFilePath)){

            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename="'.$filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fullFilePath));
            ob_clean();
            flush();
            readfile($fullFilePath);
            exit;

        }
    }

    function getEventDataAction()
    {

        $date = $this->input->get();

        echo json_encode($this->CommonModel->get_where('ministry_event_calendar', "`date` BETWEEN '{$date['from']}' AND '{$date['to']}'"));
    }

    public function ListTopAction()
    {
        echo json_encode($this->publicationRepository->FindTop(2));
    }

}