<?php

use Ministry\Model\AjaxGrid;
use Ministry\Repositories\NewsRepository;

class NewsController extends CI_Controller
{

    private $newsRepository;

    public function __construct()
    {
        parent::__construct();

        $this->newsRepository = new NewsRepository();

    }

    public function IndexAction()
    {
        if (isset($_GET['keywords'])) {

            $keywords = $_GET['keywords'];

            $params['Keywords'] = $keywords;

            $keywords = explode(' ', $keywords);

            $params['TotalCount'] = $this->newsRepository->RowCount($keywords);

        } else {
            $params['TotalCount'] = $this->newsRepository->RowCount();
        }

        $this->load->view('news', $params);
    }

    public function ListAction()
    {

        $page = isset($_GET['page']) ? $_GET['page'] : '1';
        $perPage = isset($_GET['perPage']) ? $_GET['perPage'] : '5';

        if (isset($_GET['keywords'])) {
            $keywords = $_GET['keywords'];

            $keywords = explode(' ', $keywords);

            echo json_encode($this->newsRepository->GetDataByPage($page, $perPage, $keywords));
        } else {
            echo json_encode($this->newsRepository->GetDataByPage($page, $perPage));
        }
    }

    public function ShowDetailAction()
    {
        $RequestData = $_GET['meta'];

        $RequestData = explode(" ", $RequestData);

        $RequestData = $RequestData[0];

        $params['News'] = $this->newsRepository->GetFulldata($RequestData);

        $this->load->view('news_details', $params);
    }

    public function ListTopAction()
    {
        echo json_encode($this->newsRepository->FindTop());
    }

}