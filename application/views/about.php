<?php include('includes/meta-header.php'); ?>
	<body>
		<div id="wrapper">
			<?php include('includes/nav.php'); ?>

			<div id="main">
				<div class="breadcrumb-section">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h1>About Us</h1>
								<ul class="breadcrumb">
									<li>
										<a href="<?php echo BASE_URL();?>">Home</a>
									</li>
									<li class="active">
										About Us
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="content-wrapper" id="page-info">
					<div class="container">
						<section class="our-story row">
							<div class="col-xs-12">
								<header class="story-heading section-header">
									<h2>Lorem ipsum? <strong>Dico vituperata</strong></h2>
								</header>
						
								<div class="row">

									<div class="col-xs-12 col-sm-5">
										<section class="slider-wrap flex-slide flexslider">
											<ul class="slides">
											<li>
												<img src="<?php echo BASE_URL();?>assets/img/img-7.jpg" alt="">
											</li>
											<li>
												<img src="<?php echo BASE_URL();?>assets/img/img-7.jpg" alt="">
											</li>
											</ul>
										</section>
									</div>
									<div class="col-xs-12 col-sm-7">
										<strong class="article-sammury">

                                            Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec, eum in dico noluisse. Per at viderer aliquando,
                                            putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam mandamus. Duo movet vidisse nusquam an.
									</div>
								</div>
							</div>
						</section>

						<section class="our-works row anim-section">
							<div class="col-xs-12">
								<header class="work-block-heading section-header">
									<h2>Lorem <strong>movet munere nusquam</strong></h2>
								</header>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-3">
										<div class="thumbnail zoom">
											<h3>Lorem Ipsum</h3>
											<a href="#" class="img-thumb">
												<figure>
												<img src="<?php echo BASE_URL();?>assets/img/world-population.jpg" alt="">
												</figure>
												</a>
											<p>
                                                Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec, eum in dico noluisse. Per at viderer aliquando,
                                                putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam mandamus. Duo movet vidisse nusquam an.
											</p>
											<p>
												<a href="#" class="btn btn-default btn-sm" role="button">READ MORE</a>
											</p>
										</div>
									</div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail zoom">
                                            <h3>Lorem Ipsum</h3>
                                            <a href="#" class="img-thumb">
                                                <figure>
                                                    <img src="<?php echo BASE_URL();?>assets/img/world-population.jpg" alt="">
                                                </figure>
                                            </a>
                                            <p>
                                                Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec, eum in dico noluisse. Per at viderer aliquando,
                                                putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam mandamus. Duo movet vidisse nusquam an.
                                            </p>
                                            <p>
                                                <a href="#" class="btn btn-default btn-sm" role="button">READ MORE</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail zoom">
                                            <h3>Lorem Ipsum</h3>
                                            <a href="#" class="img-thumb">
                                                <figure>
                                                    <img src="<?php echo BASE_URL();?>assets/img/world-population.jpg" alt="">
                                                </figure>
                                            </a>
                                            <p>
                                                Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec, eum in dico noluisse. Per at viderer aliquando,
                                                putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam mandamus. Duo movet vidisse nusquam an.
                                            </p>
                                            <p>
                                                <a href="#" class="btn btn-default btn-sm" role="button">READ MORE</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail zoom">
                                            <h3>Lorem Ipsum</h3>
                                            <a href="#" class="img-thumb">
                                                <figure>
                                                    <img src="<?php echo BASE_URL();?>assets/img/world-population.jpg" alt="">
                                                </figure>
                                            </a>
                                            <p>
                                                Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec, eum in dico noluisse. Per at viderer aliquando,
                                                putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam mandamus. Duo movet vidisse nusquam an.
                                            </p>
                                            <p>
                                                <a href="#" class="btn btn-default btn-sm" role="button">READ MORE</a>
                                            </p>
                                        </div>
                                    </div>
						</section>
					</div>

				</div>

			</div>

		<?php include('includes/footer.php'); ?>

		</div>


		<?php include('includes/meta-footer.php'); ?>
	