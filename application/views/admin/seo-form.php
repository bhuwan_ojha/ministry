
<?php
include"includes/base.php";
$isNew = true;
if(segment(3) != '') {
    $seo = $seo[0];
    $isNew = false;
    $name = $seo['name'];
    $id = $seo['id'];
    $title = $seo['title'];
    $link = $seo['link'];
    $description = $seo['description'];
    $keywords = $seo['keywords'];
}

?>

<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">SEO Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></a></li>
            </ul>
        </div>
    </div>
    <div class="row animated fadeInRight">
                <div class="col-sm-8">
                <form action="<?php echo BASE_URL();?>admin/save-update" method="post" id="seo-form" class="forms" style="margin-left: 30px;">
                    <input type="hidden" name="id" value="<?php echo !$isNew ? $id : '' ?>">
                            <div class="form-group">
                                <label>Page Name<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page name" class="form-control validate[required]" name="name" value="<?php echo !$isNew ? $name : '' ?>">
                            </div>


                            <div class="form-group">
                                <label>Link<span class="text-danger"></span></label>
                                <input type="text" name="link" placeholder="Enter url for seo (leave blank for home page)" class="form-control " value="<?php echo !$isNew ? $link : '' ?>">
                            </div>


                            <div class="form-group">
                                <label>Page Title<span class="text-danger">*</span></label>
                                <input type="text" placeholder="Enter page title" class="form-control validate[required]" name="title" value="<?php echo !$isNew ? $title : '' ?>">
                            </div>


                            <div class="form-group">
                                <label>Description<span class="text-danger">*</span></label>
                                <textarea name="description" placeholder="Enter page description" class="form-control validate[required]"><?php echo !$isNew ? $description : '' ?></textarea>
                            </div>


                            <div class="form-group">
                                <label>Keywords<span class="text-danger">*</span></label>
                                <textarea name="keywords" placeholder="Enter keywords (comma separated)" class="form-control validate[required]"><?php echo !$isNew ? $keywords : '' ?></textarea>
                            </div>


                            <div class="form-group">
                                <button class="btn btn-success" type="submit" ><?php echo ($isNew) ? 'Save' : 'Update' ?></button>
                            </div>
                </form>
                </div>
            </div>
        </div>


<?php include "includes/footer.php";?>
<script>

    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.left-menu-seo').removeClass('close-item');
        $('.left-menu-seo').addClass('open-item');
        $('.seo').addClass('active-item');
    });
    $(function () {
        $('#seo-form').validationEngine();

        $('#seo-form').on("submit", function (e) {

        });
    });

</script>