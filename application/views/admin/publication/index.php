<?php include(APPPATH . 'views/admin/includes/base.php'); ?>
<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Publication</a></li>
            </ul>
        </div>
    </div>

    <div class='add'>
        <button type="submit" class="btn btn-rounded btn-success pull-right "
                onclick="showAddNewForm('Add Publication','<?php echo base_url() ?>admin/publication/form',800,315)">
            <small class="glyphicon glyphicon-plus-sign"></small>
            Add
        </button>
    </div>
    <div class="clearfix"></div>

    <table style="margin-top: 10px" class="table table-bordered table-sorted" id="publication-list">
        <thead>
        <tr>

            <th width="300"><a class="table-header" field-name="short_title">Title</a></th>
            <th width="215"><a class="table-header" field-name="date">Date</a></th>
            <th><a class="table-header" field-name="media_file_name">FileName</a></th>
            <th><a class="table-header" field-name="image_file_name">Image</a></th>
            <th width="215">Actions</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div id="loading-msg-publication" class="loading-image"></div>


</div>
<?php include(APPPATH . 'views/admin/includes/footer.php'); ?>


<script>

    $(function () {
        $(".left-nav").find(".active").removeClass("active");
        $('.publication').addClass('active-item');

        $('#publication-list').ajaxGrid({
            pageSize: 5,
            defaultSortExpression: 'title',
            defaultSortOrder: 'DESC',
            tableHeading: '.table-header',
            url: '<?php echo base_url()?>admin/publication/list',
            requestType: 'get',
            loadingImage: $('#loading-msg-publication'),
            postContent: [
                {

                    control: $('<button name ="EditUser"   type="button" title="Edit" class="btn btn-info btn-rounded" onclick=\'showEditForm(this,' +
                        '"Edit","<?php echo  base_url()?>admin/publication/form",800,315)\'>' +
                        '<span class="glyphicon glyphicon-pencil"></span></button>')
                },
                {
                    control: $("<form style='display: inline-block' action='<?php echo base_url()?>admin/publication/delete' method='POST'>" +
                        "<input type='hidden' name='ID' id='ID' /> " +
                        '<button name="Delete" type="submit" title="Delete" class="btn btn-rounded btn-danger" onclick=\'return Confirmation(this,"Delete","Are you sure you want to delete?", "Yes", "No")\'>' +
                        '<small class="glyphicon glyphicon-trash"></small>' +
                        '</button></form>'),
                    properties: [
                        {
                            propertyField: 'input[type=hidden]#ID',
                            property: 'value',
                            propertyValue: 'ID'
                        }
                    ]
                }
            ],
            id: 'ID',
            NoRecordsFound: 'No Records Found',
            Previous: 'Previous',
            Next: 'Next'
        });

    });
</script>

