<?php
/**
 * @var $Publication \Ministry\Model\Publication
 * @var $SaveUpdate
 */ ?>

<style>
    .media_linkformError.parentFormPublicationEditForm.formError.formErrorInsideDialog {
        left: 0 !important;
    }
</style>

<form class="form-horizontal" id="PublicationEditForm" action="<?php echo base_url() ?>admin/publication/saveupdate"
      method="post" enctype="multipart/form-data">
    <input type="hidden" id="ID" value="<?php echo $Publication->ID ?>" name="ID"/>

    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label" for="title">Title </label>
        </div>
        <div class="col-sm-8">
            <input type="text" class="form-control validate[required]" id="title" name="title" placeholder=""
                   style="width: 90%;"
                   value="<?php echo $Publication->title ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-4">
            <label class="control-label" for="date">Date </label>
        </div>
        <div class="col-sm-8">
            <input type="text" class="form-control validate[required]" id="date" name="date" readonly
                   value="<?php echo $Publication->date ?>"
                   style="width: 90%;display: inline-block">
            <button type="button" style="display: inline-block" onclick="ToggleDate(this)"><span
                    class="glyphicon glyphicon-calendar"></span></button>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <div class="col-sm-4">
            <label class="control-label" for="editor">Upload Image : </label>
        </div>
        <div class="col-sm-2">
            <input type="file" id="image_link" name="image_link" accept="image/*"/>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <div class="col-sm-4">
            <label class="control-label" for="editor">Upload file : </label>
        </div>
        <div class="col-sm-2">
            <input type="file" id="media_link" name="media_link">
        </div>
    </div>

    <div class="form-group error-file-upload" style="margin: 0px;visibility: hidden">
        <div class="col-sm-8 col-sm-offset-4">
            <p class="c" style="color: red">Please make sure to update both the file.</p>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12" style="text-align: right">
            <button type="submit" id="SavePublicationForm"
                    class="btn btn-wide btn-success"><?php echo $SaveUpdate ?></button>
            <button type="button" class="btn btn-wide btn-danger" onclick="closeDialog(this)">Cancel
            </button>
        </div>
    </div>
</form>


<script>

    function ToggleDate(thisObj) {
        $("input#date").focus();
    }

    $(function () {

        var SaveUpdate = "<?php echo $SaveUpdate?>".trim().toLowerCase();

        $("#media_link,#image_link").filestyle();

        $("#PublicationEditForm").validationEngine();

        $("#date").datepicker({
            format: 'dd M yy ( DD )',
            autoclose: true
        });

        var date = decodeURI("<?php echo $Publication->date ?>");
        /*if (date.length > 0)*/
        $("#date").datepicker("update", new Date(date));

        $("#SavePublicationForm").on('click', function (e) {
            e.preventDefault();
            var flag = false;
            var medialink_filename = $("#media_link").val();
            var imagelink_filename = $("#image_link").val();
            if (SaveUpdate == "save") {
                if (medialink_filename.length > 0 && imagelink_filename.length > 0) {
                    flag = true;
                }
            }
            else if (SaveUpdate == "update") {
                flag = false;
                if (medialink_filename.length > 0 && imagelink_filename.length > 0) {
                    flag = true;
                }
                else if (medialink_filename.length > 0) {
                    flag = Confirmation(this, "Warning!!!", "Are you sure you want to save with old image file?", "Yes", "No");
                }
                else if (imagelink_filename.length > 0) {
                    flag = Confirmation(this, "Warning!!!", "Are you sure you want to save with old media file?", "Yes", "No");
                }
                else {
                    flag = Confirmation(this, "Warning!!!", "Are you sure you want to save with both old file?", "Yes", "No");
                }
            }
            if (flag) {
                $(".error-file-upload").css("visibility", "hidden");
                $("#PublicationEditForm").submit();
            }
            else {
                $(".error-file-upload").css("visibility", "visible");
            }

        })

    });
</script>

