<?php include('includes/base.php'); ?>
<?php successflash();?>
<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Common Meta Tags</a></li>
            </ul>
        </div>
    </div>
    <div class="row animated fadeInRight">
                    <div class="row">
                        <div class="col-sm-8">
                            <form action="" method="post" id="meta-form" class="forms">
                                <table class="table table-bordered">

                                    <tbody>

                                        <div class="form-group">
                                            <label>Language<span class="text-danger">*</span></label>
                                            <input type="text" name="language" placeholder="(eg: en, en-gb, en-au)" class="form-control validate[required]" value="<?php echo $data[0]['language'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Robots<span class="text-danger">*</span></label>
                                            <input type="text" name="robots" placeholder="(eg: all, index, follow)" class="form-control validate[required]" value="<?php echo $data[0]['robots'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Copyright<span class="text-danger">*</span></label>
                                            <input type="text" name="copyright" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['copyright'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Revisit After<span class="text-danger">*</span></label>
                                            <input type="text" name="revisit" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['revisit'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Page Topic<span class="text-danger">*</span></label>
                                            <input type="text" name="topic" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['topic'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Audience<span class="text-danger">*</span></label>
                                            <input type="text" name="audience" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['audience'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Expires<span class="text-danger">*</span></label>
                                            <input type="text" name="expires" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['expires'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Geo Placename<span class="text-danger">*</span></label>
                                            <input type="text" name="placename" placeholder="(eg: Town, Area Post/Zip, Country)" class="form-control validate[required]" value="<?php echo $data[0]['placename'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Geo Region<span class="text-danger">*</span></label>
                                            <input type="text" name="region" placeholder="(eg: COUNTRY-AREA)" class="form-control validate[required]" value="<?php echo $data[0]['region'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Geo Position<span class="text-danger">*</span></label>
                                            <input type="text" name="position" placeholder="(eg: lat, lng)" class="form-control validate[required]" value="<?php echo $data[0]['position'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Google Analytics<span class="text-danger">*</span></label>
                                            <textarea name="google_analytics" placeholder="" class="form-control validate[required]"><?php echo $data[0]['google_analytics'];?></textarea>
                                        </div>


                                        <div class="form-group">
                                            <label>Google Verification Code<span class="text-danger">*</span></label>
                                            <input type="text" name="google_site_verification" placeholder="" class="form-control validate[required]" value="<?php echo $data[0]['google_site_verification'];?>">
                                        </div>

                                        <div class="form-group">
                                            <button class="btn btn-success btn-md" type="submit" id="submit"  >Save</button>
                                        </div>

                                    </tbody>

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php include "includes/footer.php";?>
<script>
    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.left-menu-seo').removeClass('close-item');
        $('.left-menu-seo').addClass('open-item');
        $('.meta-tag').addClass('active-item');
    });
    $(function () {
        $('#meta-form').validationEngine();

        $('#meta-form').on("submit", function () {

        });
    });

</script>