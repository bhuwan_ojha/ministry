<?php include "includes/base.php"; ?>

<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Image Manager</a></li>
            </ul>
        </div>
    </div>
    <div class="row animated fadeInRight">
                <div class="card">
                    <div class="card-body card-padding">
                        <form action="<?php echo BASE_URL();?>admin/add-image" method="post" enctype="multipart/form-data">
                       <input type="file" name="file">
                        <button type="submit" class="btn btn-success" style="margin-top: 15px;">Save</button>
                        </form>
                    </div>
                </div>
        <hr>

                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <?php if($images!=0){
                            foreach ($images as $value) { ?>
                            <div class="thumbnail">
                                <img src="<?php echo BASE_URL() ?>uploads/<?php echo $value['image']; ?>" height="312px" alt="">

                                <div class="caption">
                                    <?php if ($value['status'] == 1) { ?>
                                        <h4>Banner Image</h4>
                                    <?php } else { ?> <h4>Gallery Image</h4><?php } ?>
                                    <div class="m-b-5" style="margin-top: 20px">
                                        <input type="hidden" id="image-id" value="<?php echo $value['id']; ?>">
                                        <button type="button" onclick="deleteImage(this)" class="btn btn-md btn-danger"
                                                role="button">Delete
                                        </button>
                                    </div>
                                    <div class="checkbox-custom checkbox-success" style="margin-left: 160px; margin-top: -38px;">
                                        <input type="checkbox" id="checkboxCustom3" class="ts1"  <?php if ($value['status'] == 1){ ?>checked<?php } ?>>
                                        <label class="check" for="checkboxCustom3">Activate  Banner</label>
                                    </div>



                                </div>
                            </div>
                        <?php }} ?>
                    </div>


<?php include "includes/footer.php";?>
<script>
    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.left-menu-page').removeClass('close-item');
        $('.left-menu-page').addClass('open-item');
        $('.image').addClass('active-item');
    });
    function deleteImage(thisObj) {
        var id = $('#image-id').val();
        $.ajax({
            url: '<?php echo BASE_URL();?>admin/image/delete',
            type: 'post',
            data: {id: id},
            success: function (data) {
                location.reload();
            }
        })
    }
</script>
<script>
    $(function () {
        $('.ts1').on('click', function () {
            if ($(this).is(':checked')) {
                var status = 1;
            }else{ var status = 0; }
                var id = $('#image-id').val();
                $.ajax({
                    url:'<?php echo BASE_URL();?>admin/image/update',
                    type:'post',
                    data:{id:id,status:status},
                    success: function (data) {
                       location.reload();
                    }
                });


        });
    });
</script>