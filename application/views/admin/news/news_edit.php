<?php
/**
 * @var $News \Ministry\Model\News
 * @var $SaveUpdate
 */

include(APPPATH . 'views/admin/includes/base.php');
?>
<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">News <?php echo $SaveUpdate ?></a></li>
            </ul>
        </div>
    </div>

    <form id="NewsEditForm" action="<?php echo base_url() ?>admin/news/saveupdate" method="post" enctype="multipart/form-data">
        <input type="hidden" id="ID" value="<?php echo $News->ID ?>" name="ID"/>
        <input type="hidden" name="description" id="description" value="">

        <div class="form-group">
            <label class="control-label col-sm-2" for="title">Title : </label>

            <div class="col-sm-10">
                <input type="text" class="form-control validate[required]" id="title" name="title" placeholder=""
                       value="<?php echo $News->title ?>"
                       style="width: 500px">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="date">Date : </label>

            <div class="col-sm-10">
                <input type="text" class="form-control validate[required]" id="date" name="date" readonly
                       style="width: 500px;background: white;display: inline-block;cursor: pointer">
                <button type="button" style="display: inline-block" onclick="ToggleDate(this)"><span
                        class="glyphicon glyphicon-calendar"></span></button>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="category">Category : </label>

            <div class="col-sm-10">
                <label style="margin-right: 50px;"><input type="radio" name="category" value="news" checked> news</label>
                <label><input type="radio" name="category" value="events" <?php if($News->category=='events') echo 'checked'?>> events</label>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="date">Upload image : </label>

            <div class="col-sm-10">
                <input type="file" id="upload_image" name="upload_image">
            </div>
        </div>
        <div class="form-group error-file-upload" style="margin: 0px;visibility: hidden">
            <div class="col-sm-10 col-sm-offset-2">
                <p class="c" style="color: red">Please update the file first.</p>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-12" for="editor">Description : </label>

            <div class="col-sm-12">
                <textarea class="validate[required]" id="editor" rows="10" cols="80"
                          style="width: 100%"></textarea>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group" style="margin-top: 7px">
            <div class="col-sm-12">
                <button type="submit" id="SaveNewsForm"
                        class="btn btn-wide btn-success"><?php echo $SaveUpdate ?></button>
                <button type="button" class="btn btn-wide btn-danger" onclick="Redirect('admin/news')">Cancel
                </button>
            </div>
        </div>
    </form>


</div>
<?php include(APPPATH . 'views/admin/includes/footer.php'); ?>


<script>

    var editor = null;

    function ToggleDate(thisObj) {
        $("input#date").focus();
    }

    $(function () {
        $(".left-nav").find(".active").removeClass("active");
        $('.news').addClass('active-item');

        $("#NewsEditForm").validationEngine();

        var SaveUpdate = "<?php echo $SaveUpdate?>".trim().toLowerCase();

        $("#upload_image").filestyle();

        $("#date").datepicker({
            format: 'dd M yy ( DD )',
            autoclose: true
        });

        editor = CKEDITOR.replace('editor');

        var description = decodeURI("<?php echo $News->description ?>");
        if (description.length > 0)
            editor.setData(description);

        var date = decodeURI("<?php echo $News->date ?>");
        if (date.length > 0)
            $("#date").datepicker("update", new Date(date));

        $("#SaveNewsForm").on('click', function (e) {
            e.preventDefault();
            $("input#description").val(encodeURI(editor.getData()));

            var flag = false;
            var filename = $("#upload_image").val();
            if (SaveUpdate == "save") {
                if (filename.length > 0) {
                    flag = true;
                }
            }
            else if (SaveUpdate == "update") {
                if (filename.length > 0) {
                    flag = true;
                }
                else {
                    flag = false;
                    flag = Confirmation(this, "Warning!!!", "Are you sure you want to save with old file?", "Yes", "No");
                }
            }
            if (flag) {
                $(".error-file-upload").css("visibility", "hidden");
                $("#NewsEditForm").submit();
            }
            else {
                $(".error-file-upload").css("visibility", "visible");
            }
        })

    });
</script>

