<?php include(APPPATH . 'views/admin/includes/base.php'); ?>
    <div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">News</a></li>
            </ul>
        </div>
    </div>

    <div class='add'>
        <form action="<?php echo base_url() ?>admin/news/form" method="post">
            <button type="submit" class="btn btn-rounded btn-success pull-right ">
                <small class="glyphicon glyphicon-plus-sign"></small>
                Add
            </button>
        </form>
    </div>
    <div class="clearfix"></div>

    <table style="margin-top: 10px" class="table table-bordered table-sorted" id="news-list">
        <thead>
        <tr>

            <th width="350"><a class="table-header" field-name="short_title">Title</a></th>
            <th width="215"><a class="table-header" field-name="date">Date</a></th>
            <th><a class="table-header" field-name="short_description">Description</a></th>
            <th width="215">Actions</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div id="loading-msg-news" class="loading-image"></div>


</div>
<?php include(APPPATH . 'views/admin/includes/footer.php'); ?>


<script>

    function showDescriptionFullContent(thisObj){

        var fulldescription=$(thisObj).closest("td").find(".information .full_description").val();
        ShowFullDesription('Full desrcription!!!',fulldescription,'Ok','Cancel');
    }

    $(function () {
        $(".left-nav").find(".active").removeClass("active");
        $('.news').addClass('active-item');

        $('#news-list').ajaxGrid({

            pageSize: 5,
            defaultSortExpression: 'title',
            defaultSortOrder: 'DESC',
            tableHeading: '.table-header',
            url: '<?php echo base_url()?>admin/news/list',
            requestType: 'get',
            loadingImage: $('#loading-msg-news'),
            postContent: [
                {
                    control: $('<form style="display:inline-block" action="<?php echo base_url()?>admin/news/form" method="post">'+
                        '<button type="submit" name ="Edit Option" title="Edit" type="button" class="btn btn-rounded btn-info"\'>' +
                        '<input type="hidden" name="ID" id="ID" /> ' +
                        '<small class="glyphicon glyphicon-pencil"></small>' +
                        '</button></form>'),
                    properties: [
                        {
                            propertyField: 'input[type=hidden]#ID',
                            property: 'value',
                            propertyValue: 'ID'
                        }
                    ]
                },
                {
                    control: $("<form style='display: inline-block' action='<?php echo base_url()?>admin/news/delete' method='POST'>" +
                        "<input type='hidden' name='ID' id='ID' /> " +
                        '<button name="Delete" type="submit" title="Delete" class="btn btn-rounded btn-danger" onclick=\'return Confirmation(this,"Delete","Are you sure you want to delete?", "Yes", "No")\'>' +
                        '<small class="glyphicon glyphicon-trash"></small>' +
                        '</button></form>'),
                    properties: [
                        {
                            propertyField: 'input[type=hidden]#ID',
                            property: 'value',
                            propertyValue: 'ID'
                        }
                    ]
                }
            ],
            contentAdditionalProperty: [
                {
                    name: 'short_description',
                    control: $('<div class="DescriptionSection" style="display:block;">' +
                        '<textarea class="description_section" readonly></textarea>' +
                        '<button class="btn btn-default pull-right" onclick="showDescriptionFullContent(this)">' +
                        '<span class="glyphicon glyphicon-option-horizontal"></span>' +
                        '</button>' +
                        '</div>' +
                        '<div class="information" class="hidden">' +
                        '<input type="hidden" class="full_description">' +
                        '</div>'
                    ),
                    properties: [
                        {field: 'input[type=hidden].full_description', value: 'description'},
                        {field: 'textarea.description_section', value: 'short_description'},
                    ]
                }
            ],
            id: 'ID',
            NoRecordsFound: 'No Records Found',
            Previous: 'Previous',
            Next: 'Next'
        });

    });
</script>

