<!doctype html>
<html lang="en" class="fixed">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title><?php echo SITE_NAME; ?></title>
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo BASE_URL();?>favicon.ico">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/stylesheets/css/style.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/stylesheets/css/validationEngine.jquery.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/skin-custom.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>assets/css/bootstrap-datepicker.css.map">
    <script src="<?php echo BASE_URL();?>admin-assets/ckeditor/ckeditor.js"></script>

    <script>
        var BASE_URL = '<?php echo BASE_URL();?>';
    </script>


    <script src="<?php echo BASE_URL();?>admin-assets/vendor/jquery/jquery-1.12.3.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/javascripts/template-script.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/javascripts/template-init.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/vendor/toastr/toastr.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/javascripts/examples/dashboard.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/javascripts/jquery.validationEngine.js"></script>
    <script src="<?php echo BASE_URL();?>admin-assets/javascripts/jquery.validationEngine-en.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/jquery-ui.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/ajaxgrid.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/shared.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo BASE_URL();?>assets/js/bootstrap-filestyle.min.js"></script>


</head>

<body>
<div class="wrap">
<div class="page-header">
<div class="leftside-header">
    <div class="logo">
        <a href="" class="on-click">
            <h1 style="color: #ffffff;margin-top: 5px;margin-left: 12px;">MHE</h1>
        </a>
    </div>
    <div id="menu-toggle" class="visible-xs toggle-left-sidebar" data-toggle-class="left-sidebar-open" data-target="html">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>
<div class="rightside-header">
<div class="header-middle"></div>
<div class="header-section" id="search-headerbox">
    <input type="text" name="search" id="search" placeholder="Search...">
    <i class="fa fa-search search" id="search-icon" aria-hidden="true"></i>
    <div class="header-separator"></div>
</div>

<div class="header-section" id="user-headerbox">
    <div class="user-header-wrap">
        <div class="user-photo">
            <img src="<?php echo BASE_URL();?>admin-assets/images/user-avatar.jpg" alt="Jane Doe" />
        </div>
        <div class="user-info">
            <span class="user-name"><?php $session = $this->session->all_userdata(); echo $session['username']; ?></span>
            <span class="user-profile" style="color: #000000">Administrator</span>
        </div>

    </div>
</div>
<div class="header-separator"></div>
<div class="header-section">
    <a href="<?php echo BASE_URL();?>admin/logout" data-toggle="tooltip" data-placement="left" title="Logout"><i class="fa fa-sign-out log-out" aria-hidden="true"></i></a>
</div>
</div>
</div>
<div class="page-body">
    <div class="left-sidebar">
        <div class="left-sidebar-header">
            <div class="left-sidebar-title">Navigation</div>
            <div class="left-sidebar-toggle c-hamburger c-hamburger--htla hidden-xs" data-toggle-class="left-sidebar-collapsed" data-target="html">
                <span></span>
            </div>
        </div>
        <div id="left-nav" class="nano">
            <div class="nano-content">
                <nav>
                    <ul class="nav" id="main-nav">
                        <li class="dashboard"><a href="<?php echo BASE_URL();?>admin/"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li>
                        <li class="has-child-item close-item left-menu-page" >
                            <a><i class="fa fa-cubes" aria-hidden="true"></i><span>Content Manager</span></a>
                            <ul class="nav child-nav level-1">
                                <li class="page"><a href="<?php echo BASE_URL();?>admin/content">Pages</a></li>
                                <li class="image"><a href="<?php echo BASE_URL();?>admin/banner">Banners & Gallery</a></li>
                            </ul>
                        </li>
                        <li class="has-child-item close-item left-menu-seo">
                            <a><i class="fa fa-pie-chart" aria-hidden="true"></i><span>SEO</span> </a>
                            <ul class="nav child-nav level-1">
                                <li class="seo"><a href="<?php echo BASE_URL();?>admin/seo-manager">SEO Manager</a></li>
                                <li class="meta-tag"><a href="<?php echo BASE_URL();?>admin/meta-tag">Common Meta Tags</a></li>
                            </ul>
                        </li>
                        <!--<li class="testimonial"><a href="<?php /*echo BASE_URL();*/?>admin/testimonial"><i class="fa fa-home" aria-hidden="true"></i><span>Testimonials</span></a></li>-->
                        <li class="user"><a href="<?php echo BASE_URL();?>admin/user"><i class="fa fa-home" aria-hidden="true"></i><span>User Manager</span></a></li>

                        <li class="news"><a href="<?php echo BASE_URL();?>admin/news"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>News</span></a></li>
                        <li class="publication"><a href="<?php echo BASE_URL();?>admin/publication"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>Publication</span></a></li>
                            </ul>
                </nav>
            </div>
        </div>
    </div>


