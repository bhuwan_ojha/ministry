
<?php include('includes/meta-header.php'); ?>
<body>
<style>
    .contact-form .form-group.error .form-control, .contact-form .form-group.error label, .contact-form .error .form-control {
        border-color: #ff0000;
    }
    #success{
        display:none;
        position: absolute;
        top: -48px;
        left: 0;
    }

</style>

<div id="wrapper">
    <?php include('includes/nav.php'); ?>

    <div id="main">
        <div class="breadcrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Thematic Area</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo BASE_URL();?>">Home</a>
                            </li>
                            <li class="active">
                                Thematic Area
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper container" id="page-info">
            <div class="row">


            </div>
        </div>
    </div>


    <?php include('includes/footer.php'); ?>
</div>
<?php include('includes/meta-footer.php'); ?>