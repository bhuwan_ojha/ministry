<?php include('includes/meta-header.php');
$imageDirectoryPath = base_url() . "uploads/image_link/";
?>
<body>

<div id="wrapper">
    <?php include('includes/nav.php'); ?>

    <div id="main">
        <div class="breadcrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Our News</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo BASE_URL(); ?>">Home</a>
                            </li>
                            <li class="active">
                                News
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper container" id="page-info">

            <div id="news_contents">
                <ul>

                </ul>
            </div>


            <ul id="pagination-demo" class="pagination pagination-sm pull-right"></ul>

        </div>
    </div>

    <?php include('includes/footer.php'); ?>

</div>

<script type="text/javascript">

    var PerPage = 5;

    var monthNames = ["Janaury", "February ", "March", "April", "May", "June", "July", "August", "september", "October", "November", "December"];

    var keywords = null;

    function RequestData(page) {
        $.ajax({
            url: "<?php echo base_url() ?>news/list",
            dataType: "json",
            data: {'page': page, 'perPage': PerPage, 'keywords': keywords},
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("div#news_contents").find("ul").html("");
                $.each(data, function (k, v) {
                    var date = new Date(v.date);

                    var addHtml = "<a href='<?php echo base_url() ?>news/details?meta=" + v.ID + "'>" +
                        "<li class='articleli'>" +
                        "<article class='OuterWrapper'>" +
                        "<div class='innerImage'>" +
                        "<img class='thumbnail_news' src='<?php echo $imageDirectoryPath?>"+ v.image_link+"'>" +
                        "</div>" +
                        "<div class='innerWrapper'>" +
                        "<p class='innerDate'>" + monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + "</p>" +
                        "<p class='innerTitle'><span class='glyphicon glyphicon-pushpin'></span>" + v.title + "</p>" +
                        "</div>" +
                        "</article>" +
                        "</li>" +
                        "</a>";
                    $("div#news_contents").find("ul").append(addHtml);
                });


            }
        });
    }

    $(function () {

        keywords = "<?php echo isset($Keywords)?$Keywords:null?>";

        var TotalPages = Math.ceil('<?php echo $TotalCount?>' / PerPage);

        if (TotalPages > 0) {
            $('#pagination-demo').twbsPagination({
                totalPages: TotalPages,
                visiblePages: PerPage,
                onPageClick: function (event, page) {
                    $('#page-content').text('Page ' + page);
                    RequestData(page);

                }
            });
        }


    });

</script>

<?php include('includes/meta-footer.php'); ?>


	