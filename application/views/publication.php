<?php include('includes/meta-header.php');
$imageDirectoryPath = base_url() . "uploads/image_link/";?>
    <body>
<style>
    .contact-form .form-group.error .form-control, .contact-form .form-group.error label, .contact-form .error .form-control {
        border-color: #ff0000;
    }

    #success {
        display: none;
        position: absolute;
        top: -48px;
        left: 0;
    }

</style>

<div id="wrapper">
    <?php include('includes/nav.php'); ?>

    <div id="main">
        <div class="breadcrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Publication</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url(); ?>">Home</a>
                            </li>
                            <li class="active">
                                Publication
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper container" id="page-info">
            <div class="row">
                <div class="col-sm-7" id="publication_area">
                    <div id="publication_contents">
                        <ul>
                        </ul>
                    </div>


                    <ul id="pagination-demo" class="pagination pagination-sm pull-right"></ul>
                </div>
                <div class="col-sm-5" id="publication_calendar_area">
                    <div id="calendar"></div>
                </div>


            </div>
        </div>
    </div>

    <script>

        var PerPage = 5;

        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "sep", "Oct", "Nov", "Dec"];

        function submitForm(thisObj) {
            $(thisObj).submit();
        }

        function RequestData(page) {
            $.ajax({
                url: "<?php echo base_url() ?>publication/list",
                dataType: "json",
                data: {'page': page, 'perPage': PerPage},
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("div#publication_contents").find("ul").html("");
                    $.each(data, function (k, v) {
                        var date = new Date(v.date);

                        var addHtml = "<form onclick='submitForm(this)' action='<?php echo base_url()?>publication/details' method='post'>" +
                            "<input type='hidden' name='id' value='" + v.ID + "'" +
                            "<li class='articleli'>"+
                            "<article class='OuterWrapper'>"+
                            "<div class='innerImage'>"+
                            "<img class='thumbnail_news' src='<?php echo $imageDirectoryPath?>"+ v.image_file_name+"'>"+
                            "</div>"+
                            "<div class='innerWrapper'>"+
                            "<p class='innerDate'>" + monthNames[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear() + "</p>"+
                            "<p class='innerTitle'><span class='glyphicon glyphicon-pushpin'>" + v.title + " </p>"+
                            "</div>"+
                            "</article>"+
                            "</li>"+
                            "</form>";
                        $("div#publication_contents").find("ul").append(addHtml);
                    });


                }
            });
        }

        $(function () {

            $('#calendar').eventCalendar({
                url: '<?php echo  base_url() ?>publication/getEventData',
                language: 'English',
                baseUrl: '<?php echo  base_url() ?>'
            });

            var TotalPages = Math.ceil('<?php echo $TotalCount?>' / PerPage);

            if (TotalPages > 0) {
                $('#pagination-demo').twbsPagination({
                    totalPages: TotalPages,
                    visiblePages: PerPage,
                    onPageClick: function (event, page) {
                        $('#page-content').text('Page ' + page);
                        RequestData(page);

                    }
                });
            }
        })


    </script>


    <?php include('includes/footer.php'); ?>
</div>
<?php include('includes/meta-footer.php'); ?>