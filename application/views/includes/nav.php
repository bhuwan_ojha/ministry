<header id="header" class="header-second">
    <div class="container hidden-xs">
        <div class="row primary-header">

            <div class="social-links col-xs-12 col-sm-9 col-md-9" style="height: 100%">
                <ul class="social-icons">
                    <li>
                        <a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="http://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li>
                        <a href="http://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="http://dribble.com/" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="http://pinterest.com/" target="_blank"><i class="fa fa-vimeo-square"></i></a>
                    </li>
                </ul>
            </div>
            <div class="search-filter col-xs-12 col-sm-3 col-md-3">
                <form action="<?php echo BASE_URL(); ?>news" method="get">
                    <input class="search-input" name="keywords" type="text" placeholder="Search...">
                    <button type="submit" class="btn btn-search-custome">Search</button>
                </form>
            </div>


        </div>
    </div>
    <div class="navbar navbar-default" role="navigation" style="background-color: #10398B;">
        <div class="nav-content">
            <div class="container">
                <a href="<?php echo BASE_URL(); ?>" class="brand brand-logo" title=""><img
                        src="<?php echo BASE_URL(); ?>assets/logo.png"
                        alt=""></a>
                <span class="brand brand-text" style="margin: 10">Ministry of Population and Environment<br/>National Adaptation Plan </span>


                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav" id="navbar-tour">
                            <li>
                                <a href="<?php echo BASE_URL(); ?>home" class="submenu-icon" style="color:#FFF;">Home
                                    <span class="glyphicon glyphicon-chevron-down"></span> <span
                                        class="glyphicon glyphicon-chevron-up"></span> </a>
                            </li>

                            <li>
                                <a href="<?php echo BASE_URL(); ?>about" class="submenu-icon" id="cityclick"
                                   style="color:#FFF;">About <span class="glyphicon glyphicon-chevron-down"></span>
                                    <span class="glyphicon glyphicon-chevron-up"></span> </a>
                                <ul id='citydrop' class='dropdown-content'>

                                    <li><a href="#">Background</a></li>
                                    <li><a href="#">Funding Partners</a></li>
                                    <li><a href="#">NAP Team</a></li>

                                </ul>
                            </li>


                            <li>
                                <a href="<?php echo BASE_URL(); ?>thematic-areas" class="submenu-icon"
                                   style="color:#FFF;">Thematic Areas <span
                                        class="glyphicon glyphicon-chevron-down"></span> <span
                                        class="glyphicon glyphicon-chevron-up"></span> </a>
                            </li>
                            <li>
                                <a href="<?php echo BASE_URL(); ?>news" class="submenu-icon" style="color:#FFF;">News
                                    <span class="glyphicon glyphicon-chevron-down"></span> <span
                                        class="glyphicon glyphicon-chevron-up"></span> </a>
                            </li>
                            <li>
                                <a href="<?php echo BASE_URL(); ?>publication" class="submenu-icon" style="color:#FFF;">Publication
                                    <span class="glyphicon glyphicon-chevron-down"></span> <span
                                        class="glyphicon glyphicon-chevron-up"></span> </a>
                            </li>
                            <li>
                                <a href="<?php echo BASE_URL(); ?>contact-us" style="color:#FFF;">contact us</a>
                            </li>


                        </ul>
                    </nav>

                    <div style="float: right;" class="nav-flag">
                        <img class="brand brand-logo" src="<?php echo BASE_URL(); ?>assets/img/nepal_flag.gif"
                             style="height: 80px;margin-top: -90px;">
                    </div>
                </div>
            </div>

        </div>

    </div>

</header>
<style type="text/css">

</style>


