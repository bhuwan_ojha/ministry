<!-- banner slider Start Here -->
				<section class="rev_slider_wrapperc main-slider">
					<div class="rev_slider banner-slider">
						<ul>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="500" >
								<!-- MAIN IMAGE -->
								<img src="<?php echo BASE_URL();?>assets/img/environment-day.jpg" alt="banner" data-bgfit="cover" data-bgposition="center 36%" data-bgrepeat="no-repeat">

								<div
								data-endspeed="800"
								data-easing="easeOutCirc"
								data-start="800"
								data-speed="700"
								data-y="122"
								data-x="152"
								class="tp-caption sft banner-heading">
									<h2>"Wherever you go becomes a part of you somehow."</h2>
								</div>

								<div
								data-endspeed="1000"
								data-easing="easeOutCirc"
								data-start="1000"
								data-speed="700"
								data-y="308"
								data-x="152"
								class="tp-caption sft banner-summary">
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</p>
								</div>

								<div
								data-endspeed="1200"
								data-easing="easeOutCirc"
								data-start="1200"
								data-speed="700"
								data-y="384"
								data-x="152"
								class="tp-caption sft">
									<a href="#!" class="btn btn-default">View Details</a>
								</div>
							</li>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="500" >
								<!-- MAIN IMAGE -->
								<img src="<?php echo BASE_URL();?>assets/img/human-population.jpg" alt="banner" data-bgfit="cover" data-bgposition="center 36%" data-bgrepeat="no-repeat">

								<div
								data-endspeed="800"
								data-easing="easeOutCirc"
								data-start="800"
								data-speed="700"
								data-y="122"
								data-x="152"
								class="tp-caption sft banner-heading">
									<h2>People don't take Trips Trips take people</h2>
								</div>

								<div
								data-endspeed="1000"
								data-easing="easeOutCirc"
								data-start="1000"
								data-speed="700"
								data-y="308"
								data-x="152"
								class="tp-caption sft banner-summary">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									</p>
								</div>

								<div
								data-endspeed="1200"
								data-easing="easeOutCirc"
								data-start="1200"
								data-speed="700"
								data-y="384"
								data-x="152"
								class="tp-caption sft">
									<a href="#!" class="btn btn-default">Read more</a>
								</div>

							</li>
							<!-- SLIDE  -->
							<li data-transition="random" data-slotamount="7" data-masterspeed="500" >
								<!-- MAIN IMAGE -->
								<img src="<?php echo BASE_URL();?>assets/img/slider-3.jpg" alt="banner" data-bgfit="cover" data-bgposition="center 36%" data-bgrepeat="no-repeat">

								<div
								data-endspeed="800"
								data-easing="easeOutCirc"
								data-start="800"
								data-speed="700"
								data-y="118"
								data-x="152"
								class="tp-caption sft banner-heading">
									<h2>Once a Year GO SOMEPLACE You've never BEEN BEFORE</h2>
								</div>

								<div
								data-endspeed="1000"
								data-easing="easeOutCirc"
								data-start="1000"
								data-speed="700"
								data-y="310"
								data-x="152"
								class="tp-caption sft banner-summary">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</p>
								</div>

								<div
								data-endspeed="1200"
								data-easing="easeOutCirc"
								data-start="1200"
								data-speed="700"
								data-y="400"
								data-x="152"
								class="tp-caption sft">
									<a href="#!" class="btn btn-default">Read More</a>
								</div>
							</li>
						</ul>
					</div>
				</section>
				<!-- banner slider End Here -->