<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Ministry of Population and Environment - Home</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,900|Vollkorn:400,700' rel='stylesheet'
          type='text/css'>
    <link
        href='http://fonts.googleapis.com/css?family=Lato:400,300italic,300,700%7CPlayfair+Display:400,700italic%7CRoboto:300%7CMontserrat:400,700%7COpen+Sans:400,300%7CLibre+Baskerville:400,400italic'
        rel='stylesheet' type='text/css'>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fancybox/css/fancybox.css"
          media="screen"/>
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/bootstrap-theme.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/revolution-slider/css/settings.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/global.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/responsive.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/skin.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/animate.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/skin-custom.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/event-calendar/event-calendar.css" rel="stylesheet"/>
    <link href="<?php echo base_url()?>assets/css/calendar.css" rel="stylesheet"/>

    <!--FACEBOOK-->
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <!-- Switcher Style Js -->
    <script src="<?php echo base_url()?>assets/js/jquery.cookie.js"></script>
    <!-- Bootstrap Js-->
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.easing.min.js"></script>
    <!--Main Slider Js-->
    <script src="<?php echo base_url()?>assets/revolution-slider/js/jquery.themepunch.plugins.min.js"></script>
    <script src="<?php echo base_url()?>assets/revolution-slider/js/jquery.themepunch.revolution.js"></script>
    <!--Main Slider Js End -->
    <!--Main FancyLight Js End -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/fancybox/js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/fancybox/js/custom.fancybox.js"></script>

    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/js/site.js"></script>
    <script src="<?php echo base_url()?>assets/js/moment.js"></script>
    <script src="<?php echo base_url()?>assets/js/simple-pagination.js"></script>
    <script src="<?php echo base_url()?>assets/js/twbsPagination.js"></script>
    <script src="<?php echo base_url()?>assets/event-calendar/event-calendar.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-filestyle.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/calendar.js"></script>



</head>