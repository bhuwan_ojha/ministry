<!--Footer Section Start Here -->
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="footer-logo">
								<a href="<?php echo BASE_URL();?>" title="Lorem Ipsum"><img src="<?php echo BASE_URL();?>assets/logo.png" style="height: 100px;"></a>
							</div>
							<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
							</p>
							<address>
								<span> <i class="fa fa-home"></i> <span>Loream Ipsum, Nepal</span> </span>
								<span> <i class="fa fa-phone-square"></i> <span>+0123456789</span> </span>
								<span> <i class="fa fa-envelope"></i> <span><a href="mailto:info@yourdomain.com">info@yourdomain.com</a></span> </span>
							</address>

						</div>
						<div class="col-xs-12 col-sm-4 twitter-update">
							<h6>Twitter Feed</h6>
							<p>
								<a href="#"> <span class="charity">@Lorem Ipsum</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. <span class="comment-time">2 hours ago</span> </a>
							</p>
							<p>
								<a href="#"> <span class="charity">@Lorem Ipsum</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. <span class="comment-time">2 hours ago</span> </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-4">
							<h6>Newsletter Signup</h6>
							<p>
								
Sign up your account to check our newsletter that will undoubtedly help you acquainted with current scenario.
							</p>
							<form role="form" class="sign-up">

								<div class="input-group">
									<input class="form-control" type="email" placeholder="Email">
									<div class="input-group-addon">
										<input type="submit" class="btn btn-theme" value="Submit">
									</div>
								</div>

							</form>

							<h6>Follow us</h6>
							<ul class="social-icons">
								<li>
									<a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="http://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="http://dribble.com/" target="_blank"><i class="fa fa-dribbble"></i></a>
								</li>
								<li>
									<a href="http://pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a>
								</li>
								<li>
									<a href="http://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="http://instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="copyright" style="background-color: #DC143C;">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<span style="color:#000;"> &copy; Copyright 2017, All Rights Reserved by <a href="#" target="_new"> Lorem Ipsum</a>.
									</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
