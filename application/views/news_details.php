<?php include('includes/meta-header.php'); ?>
<body>

<div id="wrapper">
    <?php include('includes/nav.php'); ?>

    <div id="main">
        <div class="breadcrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Our News</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo BASE_URL(); ?>">Home</a>
                            </li>
                            <li class="active">
                                News
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper container" id="page-info">

            <div id="article_content">
                <div class="article_header">
                    <p class="title"><?php echo $News->title?></p>
                    <p class="date"><span class="glyphicon glyphicon-time"></span> <?php echo $News->date?></p>
                </div>
                <div class="article_body">
                    <?php echo urldecode($News->description)?>
                </div>
            </div>


            <ul id="pagination-demo" class="pagination pagination-sm pull-right"></ul>

        </div>
    </div>

    <?php include('includes/footer.php'); ?>

</div>

<script type="text/javascript">

    var PerPage = 5;

    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "sep", "Oct", "Nov", "Dec"];

    function RequestData(page) {
        $.ajax({
            url: "<?php echo base_url() ?>news/list",
            dataType: "json",
            data: {'page': page, 'perPage': PerPage},
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("div#new_contents").find("ul").html("");
                $.each(data, function (k, v) {
                    var date = new Date(v.date);
                    var addHtml = "<a href='<?php echo base_url()?>news/detailsmeta=" + v.ID + "'>" +
                        "<li>" +
                        "<article class='OuterWrapper'>" +
                        "<div class='innerDate'>" +
                        "<p class='month'>" + monthNames[date.getMonth()] + "</p>" +
                        "<p class='date'>" + date.getDate() + "</p>" +
                        "<p class='year'>" + date.getFullYear() + "</p>" +
                        "</div>" +
                        "<div class='innerTitle'>" + v.title + "</div>" +
                        "</article>" +
                        "</li>" +
                        "</a>";
                    $("div#new_contents").find("ul").append(addHtml);
                });


            }
        });
    }

    $(function () {

        $('#pagination-demo').twbsPagination({
            totalPages: Math.ceil('<?php echo $TotalCount?>' / PerPage),
            visiblePages: PerPage,
            onPageClick: function (event, page) {
                $('#page-content').text('Page ' + page);
                RequestData(page);

            }
        });

    });

</script>

<?php include('includes/meta-footer.php'); ?>


	