<?php include('includes/meta-header.php');
$imageDirectoryPath = base_url() . "uploads/image_link/";
?>
<body>
<div id="wrapper">
    <?php include('includes/nav.php'); ?>

    <div id="main">
        <?php include('includes/slider.php'); ?>


        <section class="parallax-section parallax">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <h2 class="text-left">Lorem Ipsum</h2>

                        <strong class="article-sammury">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                            anim id est laborum.</strong><br><br>

                        <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                            sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                            laborum.
                        </blockquote>

                        <p class="left-text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="facebook-page">
                            <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline"
                                 data-height="450" data-small-header="false" data-adapt-container-width="true"
                                 data-hide-cover="false" data-show-facepile="true"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="our-causes our-causes-section ">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <a href="#" class="link-tour"><h2>putant quaeque lucilius</h2></a>

                                <div class="items zoom">
                                    <a href="#" class="img-thumb">

                                        <figure>
                                            <img src="#" alt="">
                                        </figure>
                                    </a>

                                    <h3 class="h4">Lorem Ipsum</h3>

                                    <p>
                                        Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec,
                                        eum in dico noluisse. Per at viderer aliquando,
                                        putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam
                                        mandamus. Duo movet vidisse nusquam an.
                                    </p>
                                    <a href="#" class="btn btn-default">READ MORE</a>

                                </div>


                            </div>
                            <div class="col-xs-12 col-md-6 cause-summary">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="#" class="link-tour"><h2>Lorem Ipsum</h2></a>
                                    </div>


                                    <div class="col-xs-12 col-sm-6 col-md-6 one-block">
                                        <div class="items zoom">
                                            <?php
                                            ?>
                                            <a href="#" class="img-thumb">
                                                <figure>
                                                    <img src="#" alt="">
                                                </figure>
                                            </a>

                                            <div class="heading-block">

                                                <h3 class="h4">Lorem Ipsum</h3>
                                            </div>
                                            <p>
                                                Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata
                                                ex nec, eum in dico noluisse. Per at viderer aliquando,
                                                putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum
                                                ex diam mandamus. Duo movet vidisse nusquam an.
                                            </p>
                                            <a data-toggle="modal" href="#" data-target=".donate-form"
                                               class="btn btn-default">READ MORE</a>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="how-to-help">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <header class="page-header section-header">
                            <h2 class="text-left">Lorem ipsum dolor sit amet? <strong>Here are some tips</strong></h2>
                        </header>

                        <div class="row help-list">
                            <div class="col-xs-12 col-sm-6 col-lg-5">
                                <article class="media">
                                    <a class="pull-left warning-icon-box" href="#"><i class="icon-volume"></i></a>

                                    <div class="media-body less-width">
                                        <h3 class="media-heading">Media</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor.
                                        </p>
                                    </div>
                                </article>
                                <article class="media">
                                    <a class="pull-left warning-icon-box" href="#"><i class="icon-user"></i></a>

                                    <div class="media-body less-width">
                                        <h3 class="media-heading">Guide</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consecteit, sed do eiusmod tempor incididunt ut
                                            labore et dolore magna aliqua.
                                        </p>
                                    </div>
                                </article>
                                <article class="media">
                                    <a class="pull-left warning-icon-box" href="#" data-toggle="modal"
                                       data-target=".donate-form"><i class="icon-heart"></i></a>

                                    <div class="media-body less-width">
                                        <h3 class="media-heading">Food & Restaurants</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqrud.

                                        </p>
                                    </div>
                                </article>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-6 col-lg-offset-1">
                                <div class="embed-responsive embed-responsive-16by9">

                                    <iframe id="nepal-video" width="640" height="360"
                                            src="https://www.youtube.com/embed/ucMIkI1pwaw" frameborder="0"
                                            allowfullscreen></iframe>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        </section>

        <section class="latest-news news-section" style="margin-bottom: -100px;">
            <div class="container anim-section">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="row">
                            <header class="col-xs-12 page-header section-header clearfix">
                                <h2>Lorem Ipsum <strong>dorlor diy</strong></h2>
                            </header>
                            <div class="items zoom col-xs-12 col-md-6">
                                <div class="row">
                                    <a href="#" class="img-thumb col-xs-12 col-sm-5">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/save-environment.jpg" alt="">
                                        </figure>
                                    </a>

                                    <div class="media col-xs-12 col-sm-7">
                                        <h3>consectetur adipiscing </h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud , Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        </p>
                                        <a href="#" class="btn btn-default">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="items zoom col-xs-12 col-md-6">
                                <div class="row">
                                    <a href="#" class="img-thumb col-xs-12 col-sm-5">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/humanimpact.jpg" alt="">
                                        </figure>
                                    </a>

                                    <div class="media col-xs-12 col-sm-7">
                                        <h3>Lorem Ipsum</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud , Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        </p>
                                        <a href="#" class="btn btn-default">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="items zoom col-xs-12 col-md-6">
                                <div class="row">
                                    <a href="#" class="img-thumb col-xs-12 col-sm-5">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/world-environment-day.jpg"
                                                 width="640px" height="640px" alt="">
                                        </figure>
                                    </a>

                                    <div class="media col-xs-12 col-sm-7">
                                        <h3> sed do</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud , Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        </p>
                                        <a href="#" class="btn btn-default">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="items zoom col-xs-12 col-md-6">
                                <div class="row">
                                    <a href="#" class="img-thumb col-xs-12 col-sm-5">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/spring.jpg" alt="">
                                        </figure>
                                    </a>

                                    <div class="media col-xs-12 col-sm-7">
                                        <h3>eiusmod tempor</h3>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud ,
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        </p>
                                        <a href="#" class="btn btn-default">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="our-causes our-causes-section ">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>Lorem Ipsum <strong> dolor sit amet</strong></h2>
                            </div>

                            <div class="col-xs-12 col-md-3">
                                <div class="items zoom">
                                    <a href="#" class="img-thumb">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/world-population.jpg" alt="">
                                        </figure>
                                    </a>

                                    <h3 class="h4">Lorem Ipsum</h3>

                                    <p>
                                        Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec,
                                        eum in dico noluisse. Per at viderer aliquando,
                                        putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam
                                        mandamus. Duo movet vidisse nusquam an.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="items zoom">
                                    <a href="#" class="img-thumb">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/world-population.jpg" alt="">
                                        </figure>
                                    </a>

                                    <h3 class="h4">Lorem Ipsum</h3>

                                    <p>
                                        Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec,
                                        eum in dico noluisse. Per at viderer aliquando,
                                        putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam
                                        mandamus. Duo movet vidisse nusquam an.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="items zoom">
                                    <a href="#" class="img-thumb">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/world-population.jpg" alt="">
                                        </figure>
                                    </a>

                                    <h3 class="h4">Lorem Ipsum</h3>

                                    <p>
                                        Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec,
                                        eum in dico noluisse. Per at viderer aliquando,
                                        putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam
                                        mandamus. Duo movet vidisse nusquam an.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="items zoom">
                                    <a href="#" class="img-thumb">
                                        <figure>
                                            <img src="<?php echo BASE_URL(); ?>assets/img/world-population.jpg" alt="">
                                        </figure>
                                    </a>

                                    <h3 class="h4">Lorem Ipsum</h3>

                                    <p>
                                        Lorem ipsum dolor sit amet, movet munere nusquam ex mea. Dico vituperata ex nec,
                                        eum in dico noluisse. Per at viderer aliquando,
                                        putant quaeque lucilius an sit. Iusto accusam reprehendunt eos at, cum ex diam
                                        mandamus. Duo movet vidisse nusquam an.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="parallel-news-event-calendar" style="margin-bottom: 20px">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h4>Latest <b>News/Updates</b></h4>

                        <div id="home_news_contents">
                            <ul>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="col-xs-12">

                            <h4>Latest <b>Events</b></h4>

                            <div id="home_event_contents">
                                <ul>

                                </ul>
                            </div>

                        </div>

                        <div class="col-xs-12" class="calendar-home-page">

                            <h4 style="display: block;float: right;margin: 10px 0 4px 0 ;">Calendar</h4>

                            <div class="clearfix"></div>
                            <div style="display: block;float: right" id="calendar"></div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <script>
            var monthNames = ["Janaury", "February ", "March", "April", "May", "June", "July", "August", "september", "October", "November", "December"];
            $(function () {

                //top 4 news
                $.ajax({
                    url: "<?php echo base_url()?>news/listtoptwo",
                    data: {"category": "news", "limit": '5'},
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data = JSON.parse(data);
                        $("div#home_news_contents").find("ul").html("");
                        $.each(data, function (k, v) {
                            var date = new Date(v.date);

                            var addHtml = "<li class='articleli'>" +
                                "<article class='OuterWrapper'>" +
                                "<div class='innerImage'>" +
                                "<img class='thumbnail_news' src='<?php echo $imageDirectoryPath?>" + v.image_link + "'>" +
                                "</div>" +
                                "<div class='innerWrapper'>" +
                                "<p class='innerDate'>" + monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + "</p>" +
                                "<p class='innerTitle'>" + v.title.substr(0, 50) + "</p>" +
                                "<p class='innerDescription'>" + decodeURI(v.description).substr(0, 200) + " ...</p> " +
                                "<a class='read-more-anchor' href='<?php echo base_url() ?>news/details?meta=" + v.ID + "'>Read More</a>" +
                                "</div>" +
                                "</article>" +
                                "</li>";
                            $("div#home_news_contents").find("ul").append(addHtml);
                        });
                    }
                });

                //top 2 publication
                $.ajax({
                    url: "<?php echo base_url()?>news/listtoptwo",
                    data: {"category": "events", "limit": '2'},
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data = JSON.parse(data);
                        $("div#home_event_contents").find("ul").html("");
                        $.each(data, function (k, v) {
                            var date = new Date(v.date);

                            var addHtml = "<li class='articleli'>" +
                                "<article class='OuterWrapper'>" +
                                "<div class='innerImage'>" +
                                "<img class='thumbnail_news' src='<?php echo $imageDirectoryPath?>" + v.image_link + "'>" +
                                "</div>" +
                                "<div class='innerWrapper'>" +
                                "<p class='innerDate'>" + monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + "</p>" +
                                "<p class='innerTitle'>" + v.title.substr(0, 50) + "</p>" +
                                "<p class='innerDescription'>" + decodeURI(v.description).substr(0, 200) + " ...</p> " +
                                "<a class='read-more-anchor' href='<?php echo base_url() ?>news/details?meta=" + v.ID + "'>Read More</a>" +
                                "</div>" +
                                "</article>" +
                                "</li>";
                            $("div#home_event_contents").find("ul").append(addHtml);
                        });
                    }
                });

                // inline

                var $ca = $('#calendar').calendar({
                    width: 450,
                    height: 400,
                    monthArray: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                });

                $(".calendar-arrow").find(".prev").eq(0).click();
                $(".calendar-arrow").find(".next").eq(0).click();

            })
        </script>

        <section class="how-to-help">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <header class="page-header section-header">
                            <h2><strong>Videos</strong></h2>
                        </header>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-lg-4">
                                <div class="embed-responsive embed-responsive-16by9">

                                    <iframe width="640" height="360" src="https://www.youtube.com/embed/sW7fxGG9cEM"
                                            frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4">
                                <div class="embed-responsive embed-responsive-16by9">

                                    <iframe width="640" height="360" src="https://www.youtube.com/embed/bn8R_XqjjI0"
                                            frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-lg-4">
                                <div class="embed-responsive embed-responsive-16by9">

                                    <iframe width="640" height="360" src="https://www.youtube.com/embed/BaFpv03hq-4"
                                            frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <section class="testimonial parallax">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="testimonial-slider flexslider">
                            <ul class="slides">
                                <li>
                                    <div class="slide">
                                        <h2>Lorems Ipsums Review <strong>Lorem Ipsum</strong></h2>
                                        <blockquote>
                                            <p>
                                                â€œLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod tempor incididunt ut labore et dolore magna aliqua.â€�
                                            </p>
                                            <footer>
                                                <span>TONY VEDVIK</span>
                                                <cite>(Head Sales of Sense Technology)</cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </li>
                                <li>
                                    <div class="slide">
                                        <h2>Lorem ipsum dolor sit amet <strong>adipiscing elit</strong></h2>
                                        <blockquote>
                                            <p>
                                                â€œLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod tempor incididunt ut labore et dolore magna aliqua. â€�
                                            </p>
                                            <footer>
                                                <span>Jhony Waker</span>
                                                <cite>(CEO at NewCompany)</cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </li>
                                <li>
                                    <div class="slide">
                                        <h2>Lorem ipsum dolor sit amet <strong>adipiscing elit</strong></h2>
                                        <blockquote>
                                            <p>
                                                â€œLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod tempor incididunt ut labore et dolore magna aliqua.â€�
                                            </p>
                                            <footer>
                                                <span>TONY VEDVIK</span>
                                                <cite>(Head Sales of Sense Technology)</cite>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

    <?php include('includes/footer.php'); ?>

</div>


<?php include('includes/meta-footer.php'); ?>
	