<?php

namespace Ministry\Model;

use Ministry\Model\Admin\ModelAbstract;

class News extends ModelAbstract
{

    public $ID;

    public $title;

    public $date;

    public $image_link;

    public $description;

    public $priority;

    public $category;

}