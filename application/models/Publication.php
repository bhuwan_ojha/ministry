<?php

namespace Ministry\Model;

use Ministry\Model\Admin\ModelAbstract;

class Publication extends ModelAbstract
{

    public $ID;

    public $title;

    public $date;

    public $media_file_name;

    public $image_file_name;

}