<?php

namespace Ministry\Model;

use Ministry\Model\Admin\ModelAbstract;

class AjaxGrid extends ModelAbstract
{
    public $offset;

    public $rowNumber;

    public $sortExpression;

    public $sortOrder;

    public $page;

    public $advanceSorting;

}