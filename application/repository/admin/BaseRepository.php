<?php

namespace Ministry\Repositories\Admin;

use PDO;

class BaseRepository
{
    protected $pdo = null;

    private $modelClass;

    function __construct($modelClass)
    {
        $CI = &get_instance();
        $CI->load->database();

        $host=$CI->db->hostname;
        $user=$CI->db->username;
        $pass=$CI->db->password;
        $dbName=$CI->db->database;

        if (is_null($this->pdo)) {

            $this->pdo = new PDO('mysql:host='.$host.';dbname='.$dbName, $user, $pass, array(
                \PDO::MYSQL_ATTR_LOCAL_INFILE => true));


            $this->pdo->exec("SET CHARACTER SET utf8");
            $this->pdo->exec("SET NAMES utf8");

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $this->pdo->exec("USE {$dbName};");
        }

        $this->modelClass = $modelClass;

    }

    protected function Insert( $table,$model, $removeFields = array())
    {

        $modelArray = (array)$model;

        foreach ($removeFields as $removeField) {
            unset($modelArray[$removeField]);
        }

        $insertSql = "INSERT INTO {$table} (";

        $keys = array_keys($modelArray);

        $insertSql .= '`' . implode('`,`', $keys) . '`' . ") ";

        $insertSql .= "VALUES(";

        $insertSql .= ":" . implode(',:', $keys) . ")";

        $sqlQuery =  $this->pdo->prepare($insertSql);

        foreach ($modelArray as $key => $value) {
            $sqlQuery->bindValue(":" . $key, $value);
        }

        $sqlQuery->execute();

        return  $this->pdo->lastInsertId();
    }

    protected function UpdateTable($table, $model, $removeFields, $id = null, $updateFrom = null, $updateFromValue = null)
    {
        $modelArray = (array)$model;

        foreach ($removeFields as $removeField) {
            unset($modelArray[$removeField]);
        }

        $updateSql = "UPDATE {$table} SET ";

        $keys = array_keys($modelArray);

        foreach ($keys as $key) {
            $updateSql .= "`$key`=:$key,";
        }

        $updateSql = rtrim($updateSql, ',');

        if ($updateFrom == null) {
            if ($id == null)
                $updateSql .= " WHERE ID=:ID";
            else
                $updateSql .= " WHERE $id=:$id";
        } else
            $updateSql .= " WHERE $updateFrom=:$updateFrom";

        $sqlQuery = $this->pdo->prepare($updateSql);

        if ($updateFrom == null) {
            if ($id == null)
                $sqlQuery->bindValue(":ID", $model->ID);
            else
                $sqlQuery->bindValue(":$id", $model->$id);
        } else
            $sqlQuery->bindValue(":$updateFrom", $updateFromValue);

        foreach ($modelArray as $key => $value) {
            $sqlQuery->bindValue(":" . $key, $value);
        }

        $sqlQuery->execute();
    }

    public function GetById($table, $id, $idFieldName = null)
    {
        if ($idFieldName == null) {
            $sqlQuery = $this->pdo->prepare("SELECT * FROM {$table} WHERE ID=:ID");
            $sqlQuery->bindParam(':ID', $id);
        } else {
            $sqlQuery = $this->pdo->prepare("SELECT * FROM {$table} WHERE $idFieldName=:$idFieldName");
            $sqlQuery->bindParam(":$idFieldName", $id);
        }

        $sqlQuery->execute();

        $model = new $this->modelClass();

        while ($row = $sqlQuery->fetch(\PDO::FETCH_ASSOC)) {
            $model->MapParameters($row);
        }

        return $model;
    }

    public function Delete($table, $id, $idFieldName = null)
    {
        try {
            if ($idFieldName == null) {
                $sqlQuery = $this->pdo->prepare("DELETE FROM {$table} WHERE ID=:ID");
                $sqlQuery->bindValue("ID", $id);
            } else {
                $sqlQuery = $this->pdo->prepare("DELETE FROM {$table} WHERE $idFieldName=:$idFieldName");
                $sqlQuery->bindValue("$idFieldName", $id);
            }

            $sqlQuery->execute();

            return true;
        } catch (\Exception $e) {

            return false;
        }
    }

}