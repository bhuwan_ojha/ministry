<?php

namespace Ministry\Repositories;

use Ministry\Model\AjaxGrid;
use Ministry\Model\Publication;
use Ministry\Repositories\Admin\BaseRepository;

class PublicationRepository extends BaseRepository
{
    private $table;

    function __construct()
    {
        parent::__construct(Publication::class);

        $this->table = "ministry_publication_calendar";
    }

    public function RowCount()
    {
        $sql = "SELECT Count(*) FROM `ministry_publication_calendar`";
        $sqlQuery = $this->pdo->query($sql);
        $rowCount = $sqlQuery->fetch();
        return $rowCount[0];

    }

    public function GetDataByPage($page, $perPage)
    {
        $offset = (intval($page) - 1) * $perPage;

        $sql = "SELECT * FROM `ministry_publication_calendar` ORDER BY `title` DESC LIMIT $perPage OFFSET $offset";
        $sqlQuery = $this->pdo->query($sql);
        return $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function MapProperData($data)
    {
        $newData = array();
        foreach ($data as $eachData) {
            $filename = $eachData['media_file_name'];
            $filename = explode(".", $filename);
            $eachData["media_file_name"] = substr($filename[0], 0, -7) . "." . end($filename);

            $filename = $eachData['image_file_name'];
            $filename = explode(".", $filename);
            $eachData["image_file_name"] = substr($filename[0], 0, -7) . "." . end($filename);

            array_push($newData, $eachData);
        }
        return $newData;

    }

    public function FindAll(AjaxGrid $ajaxGrid)
    {
        $sql = "SELECT *,LEFT(`title` , 100) AS short_title FROM `ministry_publication_calendar` ORDER BY $ajaxGrid->sortExpression $ajaxGrid->sortOrder LIMIT {$ajaxGrid->rowNumber} OFFSET {$ajaxGrid->offset}";
        $sqlQuery = $this->pdo->query($sql);
        $data = $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

        $data = $this->MapProperData($data);

        $sql = "SELECT Count(*) FROM `ministry_publication_calendar`";
        $sqlQuery = $this->pdo->query($sql);
        $rowCount = $sqlQuery->fetch();

        $list['RowCount'] = $rowCount[0];
        $list['Data'] = $data;
        $list['PageNumber'] = $ajaxGrid->page;

        return $list;

    }

    public function FindTop($limit)
    {
        $sql = "SELECT * FROM `ministry_publication_calendar` ORDER BY ID DESC
              LIMIT {$limit}
              OFFSET 0";
        $sqlQuery = $this->pdo->query($sql);
        return $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function GetPublicationById($id)
    {
        try {

            return $this->GetById($this->table, $id, "ID");

        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    public function SavePublication(Publication $publication)
    {
        try {

            $publication->ID = $this->Insert($this->table, $publication, array("ID"));

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    public function UpdatePublication(Publication $publication, $media, $image)
    {
        try {
            $excludeList = array();
            array_push($excludeList, "ID");
            if (!$media) {
                array_push($excludeList, "media_file_name");
            }
            if (!$image) {
                array_push($excludeList, "image_file_name");
            }
            $this->UpdateTable($this->table, $publication, $excludeList);

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }

    }

    public function DeletePublication($Id)
    {
        try {
            $this->Delete($this->table, $Id, "ID");

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    public function GetFulldata($id)
    {
        $sql = "SELECT * FROM `ministry_publication_calendar` WHERE ID=" . $id;
        $sqlQuery = $this->pdo->query($sql);
        $data = $sqlQuery->fetchAll(\PDO::FETCH_CLASS);
        $data = $data[0];
        $data->date = date("d F, Y", strtotime($data->date));
        return $data;
    }


}