<?php

namespace Ministry\Repositories;

use Ministry\Model\AjaxGrid;
use Ministry\Model\News;
use Ministry\Repositories\Admin\BaseRepository;

class NewsRepository extends BaseRepository
{
    private $table;

    function __construct()
    {
        parent::__construct(News::class);

        $this->table = "ministry_event_calendar";
    }

    /*this part for search*/
    public function AddKeyWordsCondition($keywords)
    {
        $sql = null;
        if (count($keywords) > 0) {
            $sql .= " AND ";
            foreach ($keywords as $keyw) {

                $sql .= " title LIKE '%" . $keyw . "%' OR";
            }
        }


        $sql = substr($sql, 0, -3);
        return $sql;
    }


    /*this part for news interface pagination*/
    public function RowCount($keywords = array())
    {
        if (count($keywords) <= 0) {
            $sql = "SELECT Count(*) FROM `ministry_event_calendar` WHERE `category`='news' ";
        } else {
            $sql = "SELECT Count(*) FROM `ministry_event_calendar` WHERE `category`='news'  " . self::AddKeyWordsCondition($keywords);
        }
        $sqlQuery = $this->pdo->query($sql);
        $rowCount = $sqlQuery->fetch();
        return $rowCount[0];

    }

    /*this part for news interface*/

    public function GetDataByPage($page, $perPage, $keywords = array())
    {
        $offset = (intval($page) - 1) * $perPage;

        if (count($keywords) <= 0) {
            $sql = "SELECT * FROM `ministry_event_calendar` WHERE `category`='news'  ";
        } else {
            $sql = "SELECT * FROM `ministry_event_calendar` WHERE `category`='news'  " . self::AddKeyWordsCondition($keywords);
        }


        $sql .= " ORDER BY `title` DESC LIMIT $perPage OFFSET $offset";
        $sqlQuery = $this->pdo->query($sql);
        return $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);
    }

    /*this part for admin */
    public function FindAll(AjaxGrid $ajaxGrid)
    {
        $sql = "SELECT * FROM `ministry_event_calendar`  ORDER BY $ajaxGrid->sortExpression $ajaxGrid->sortOrder
              LIMIT $ajaxGrid->rowNumber
              OFFSET $ajaxGrid->offset";
        $sqlQuery = $this->pdo->query($sql);
        $data = $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

        $data = self::MapCorrectData($data);

        $sql = "SELECT Count(*) FROM `ministry_event_calendar`";
        $sqlQuery = $this->pdo->query($sql);
        $rowCount = $sqlQuery->fetch();

        $list['RowCount'] = $rowCount[0];
        $list['Data'] = $data;
        $list['PageNumber'] = $ajaxGrid->page;

        return $list;

    }

    /*this part for home*/

    public function FindTop()
    {
        $sql = "SELECT * FROM `ministry_event_calendar` where category='{$_GET["category"]}'  ORDER BY ID DESC
              LIMIT {$_GET["limit"]}
              OFFSET 0";
        $sqlQuery = $this->pdo->query($sql);
        $data = $sqlQuery->fetchAll(\PDO::FETCH_ASSOC);

        return self::MapCorrectData($data);

    }

    private function MapCorrectData($data)
    {
        $newData = array();
        foreach ($data as $eachData) {
            $eachData['short_description'] = strlen($eachData['description']) <= 0 ? '' : substr(urldecode($eachData['description']), 0, 200);
            $eachData['short_title'] = substr($eachData['title'], 0, 70);
            if (strlen($eachData['title']) > 70) $eachData['short_title'] .= "...";
            array_push($newData, $eachData);
        }
        return $newData;

    }

    /*these part for back end*/

    public function GetNewsById($id)
    {
        try {

            return $this->GetById($this->table, $id, "ID");

        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    public function SaveNews(News $news)
    {
        try {
            $news->priority = 1;
            $news->ID = $this->Insert($this->table, $news, array("ID"));

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    public function UpdateNews(News $news, $excludeFile = false)
    {
        try {
            $news->priority = 1;

            if ($excludeFile) {
                $this->UpdateTable($this->table, $news, array("ID", "image_link"));
            } else {
                $this->UpdateTable($this->table, $news, array("ID"));
            }

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }

    }

    public function DeleteNews($Id)
    {
        try {
            $this->Delete($this->table, $Id, "ID");

            return true;
        } catch (\Exception $e) {

            dd($e->getMessage());

            return false;
        }
    }

    /*this part for news details*/

    public function GetFulldata($id)
    {
        $sql = "SELECT * FROM `ministry_event_calendar` WHERE ID = " . $id;
        $sqlQuery = $this->pdo->query($sql);
        $data = $sqlQuery->fetchAll(\PDO::FETCH_CLASS);
        $data = $data[0];
        $data->date = date("d F, Y", strtotime($data->date));
        return $data;
    }


}