function Redirect(url) {
    window.location.href = BASE_URL + url;
}


function showAddNewForm(title, url, width, height, data, callbackOnClose, add) {
    var formDiv = $('<div></div>');


    if (data === "undefined") {
        data = null;
    }
    window.newFormDialog = formDiv.dialog({
        width: width,
        height: height,
        title: title,
        modal: true,
        resizable: false,
        close: function () {
            removeDialog(this);
            if (typeof callbackOnClose != "undefined")
                callbackOnClose(add);
        },
        open: function () {
            var closeBtn = $('.ui-dialog-titlebar-close');
            closeBtn.html('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"></span>');
        },
        drag: function (event, ui) {
            $(event.target).find('.formError').hide();
        },
        dragStart: function (event, ui) {
            $(event.target).find("input").trigger('blur');
            $(event.target).find('.formError').hide();
        },
        dragStop: function (event, ui) {
            $(event.target).find('.formError').remove();
        }
    });

    $.ajax({
        type: "get",
        url: url,
        data: {param: data},
        success: function (response) {
            formDiv.html(response);
        }
    });
}


function ShowFullDesription(title, message, OkButtonText, OkButtonCallback) {

    var OkBtnText = 'Ok';
    if (OkButtonText != undefined) {
        OkBtnText = OkButtonText;
    }
    var $WarningDialogDiv = $("<div id='dialog-warning' title='Full description' style='overflow: auto'>" +
                            "<p style='word-break: break-all'>" + decodeURI(message) + "</p></div>");
    $WarningDialogDiv.dialog({
        title: title,
        resizable: false,
        height: '500',
        width: '800',
        modal: true,
        open: function () {
            var closeBtn = $('.ui-dialog-titlebar-close');
            closeBtn.html('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>');
            closeBtn.on("click", function(){
                /*if(OkButtonCallback != undefined){
                    OkButtonCallback();
                }*/
            })
        },
        buttons: [
            {
                id: "OkDialogButton",
                text: OkBtnText,
                click: function () {
                    $(this).dialog("destroy");
                    $WarningDialogDiv.remove();
                    /*if(OkButtonCallback != undefined){
                        OkButtonCallback();
                    }*/
                }
            }
        ]
    });
}

function showEditForm(thisObj, title, url, width, height, data) {

    var id;

    if ($(thisObj).closest('tr').length) {
        id = $(thisObj).closest('tr').attr('id');
    } else {
        id = $(thisObj).closest('div[data-id]').attr('data-id');
    }

    editForm(title, url, width, height, data, id);
}

function editForm(title, url, width, height, data, id) {
    var formDiv = $('<div/>');
    formDiv.dialog({
        width: width,
        height: height,
        modal: true,
        title: title,
        /*position: { my: "center top", at: "center top+150", of: window },*/
        resizable: false,
        close: function () {
            removeDialog(this);
        },
        open: function () {
            var closeBtn = $('.ui-dialog-titlebar-close');
            closeBtn.html('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>');
        },
        dialogClass: 'dlgfixed'
    });

    $.ajax({
        type: "get",
        url: url,
        data: {'ID': id, 'param': data},
        success: function (response) {
            formDiv.html(response);
        }
    });
}

function removeDialog(thisObj) {
    $(thisObj).find('input').each(function () {
        $(this).trigger('removeErrorMessage');
    });

    $(thisObj).dialog('destroy').remove();
}

function closeDialog(thisObj) {
    $(thisObj).find('input').each(function () {
        $(this).trigger('removeErrorMessage');
    });

    $(thisObj).closest('.ui-dialog-content').dialog('close');

}

confirmed=false;

function Confirmation(obj, title, confirmMsg, OkLabel, CancelLabel) {


    //  console.log(confirmMsg);

    if (OkLabel == null)
        OkLabel = "Yes";

    if (CancelLabel == null)
        CancelLabel = "No";


    if (!confirmed) {
        $('body').append("<div id='dialog-confirm'><p>" + confirmMsg + "</p>");

        $("#dialog-confirm").dialog({
            title: title,
            resizable: false,
            height: "auto",
            width: "auto",
            open: function () {
                var closeBtn = $('.ui-dialog-titlebar-close');
                closeBtn.html('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>');
            },
            modal: true,
            buttons: [
                {
                    text: OkLabel,
                    click: function () {
                        $(this).dialog("close");
                        $('#dialog-confirm').remove();
                        confirmed = true;
                        obj.click();
                    }
                },
                {
                    text: CancelLabel,
                    click: function () {
                        $(this).dialog("close");
                        confirmed = false;
                        $('#dialog-confirm').remove();
                    }
                }
            ]

        }).parent().find(".ui-dialog-titlebar-close").click(function () {
            $('#dialog-confirm').remove();
        });

        if (CancelLabel == 'null') {
            $('.ui-dialog-buttonset').find("button:First-child").remove();
            $('.ui-dialog-buttonset').find("button:last-child").text(OkLabel)
        }

    }

    return confirmed;
}