/*
SQLyog Community v12.14 (32 bit)
MySQL - 5.6.17 : Database - ministry_health_environment
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ministry_health_environment` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `ministry_health_environment`;

/*Table structure for table `ministry_publication_calendar` */

DROP TABLE IF EXISTS `ministry_publication_calendar`;

CREATE TABLE `ministry_publication_calendar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `date` date DEFAULT NULL,
  `media_file_name` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `image_file_name` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ministry_publication_calendar` */

insert  into `ministry_publication_calendar`(`ID`,`title`,`date`,`media_file_name`,`image_file_name`) values 
(9,'test','2017-04-06','alohaK71pMhG.srt','Koala8rvVNkT.jpg'),
(10,'test','2017-04-05','Desertrw9Z48i.jpg','LighthouselWd2l80.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
